<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if ($this->session->userdata('level') == '') {
			redirect('login','refresh');
		}
		$data = array(
			'konten' => 'home',
			'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index',$data);
	}

	

	public function get_master_menu()
	{
		$id_menu = $this->input->post('id_menu');
		$rw = $this->db->get_where('master_menu', array('id_menu'=>$id_menu))->row();
		echo json_encode($rw);
	}

	public function simpan_menu_level()
	{
		$data = $this->input->post('data');
		$data_dihapus = $this->input->post('data_dihapus');
		$id_level = $this->input->post('id_level');
		$dt = json_decode($data);
		$dt_hapus = json_decode($data_dihapus);

		// log_r($_POST);
		$this->db->trans_begin();

		$this->db->where('level', $id_level);
		$this->db->delete('master_menu_level');

		$this->db->where('level', $id_level);
		$this->db->delete('module_access');

		

		foreach ($dt as $key => $value) {
			$id_parent = $dt[$key]->id;
			$nama_parent = $dt[$key]->nama;

			$this->db->where('nama_menu', $nama_parent);
			$mn = $this->db->get('master_menu')->row();

			$id_pr = 0;
			$this->db->where('nama_menu', $nama_parent);
			$this->db->where('level', $id_level);
			$cek_pr = $this->db->get('master_menu_level');

			if ($cek_pr->num_rows() > 0) {
				
				$this->db->where('nama_menu', $nama_parent);
				$this->db->where('level', $id_level);
				$this->db->update('master_menu_level', array('urutan'=>$key+1));

			} else {

				$this->db->insert('master_menu_level', array(
					'nama_menu' =>$mn->nama_menu,
					'icon'=>$mn->icon,
					'link'=>$mn->link,
					'status'=>$mn->status,
					'urutan'=>$key+1,
					'level'=>$id_level,
					'aktif'=>'y',
				));
				$id_pr = $this->db->insert_id();

				if ($mn->link !='#') {
					if ( strpos($mn->link, '/') ) {
						$link = explode('/', $mn->link);
						$module = $link[0];
						$operation = $link[1];
					} else {
						$module = $mn->link;
						$operation = 'index';
					}
					

					$this->db->insert('module_access', array(
						'level' => $id_level,
						'module' => $module,
						'operation' => $operation,
					));
				}

			}

			

			if (isset($dt[$key]->children)) {
				$chil = $dt[$key]->children;
				foreach ($chil as $key2 => $value) {
					$id_child = $chil[$key2]->id;
					$nama_child = $chil[$key2]->nama;

					$this->db->where('nama_menu', $nama_child);
					$submn = $this->db->get('master_menu')->row();

					$this->db->where('nama_menu', $nama_child);
					$this->db->where('level', $id_level);
					$cek_child = $this->db->get('master_menu_level');

					if ($cek_child->num_rows() > 0) {
						
						$this->db->where('nama_menu', $nama_child);
						$this->db->where('level', $id_level);
						$this->db->update('master_menu_level', array('urutan'=>$key2+1));

					} else {

						$this->db->insert('master_menu_level', array(
							'nama_menu' =>$submn->nama_menu,
							'icon'=>$submn->icon,
							'link'=>$submn->link,
							'status'=>$submn->status,
							'parent'=>$id_pr,
							'urutan'=>$key2+1,
							'level'=>$id_level,
							'aktif'=>'y',
						));

						if ($submn->link !='#') {
							if ( strpos($submn->link, '/') ) {
								$link = explode('/', $submn->link);
								$module = $link[0];
								$operation = $link[1];
							} else {
								$module = $submn->link;
								$operation = 'index';
							}

							$this->db->insert('module_access', array(
								'level' => $id_level,
								'module' => $module,
								'operation' => $operation,
							));
						}

					}

				}
			}


			

		}

		if ($this->db->trans_status() === FALSE)
		{
		        $this->db->trans_rollback();
		        echo "gagal server";
		}
		else
		{
		        $this->db->trans_commit();
		        echo "menu berhasil disimpan";
		}
		
		
	}

	public function set_aktif_submenu()
	{
		$id = $this->input->post('id');
		$this->session->set_userdata(array('submn_parent'=>$id));
		echo "1";
	}

	public function set_clear_submenu()
	{
		$this->session->unset_userdata('submn_parent');
	}

	public function filter_kurikulum_prodi($id_prodi)
	{
		if ($this->session->userdata('level') == '') {
			redirect('login','refresh');
		}
		?>
		<select name="id_kurikulum" id="id_kurikulum" style="width:100%;">
                <option value="">--Pilih Kurikulum --</option>
		<?php

			$this->db->where('id_prodi', $id_prodi);
			foreach ($this->db->get('kurikulum')->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_kurikulum ?>"><?php echo '['.$rw->mulai_berlaku.'] '.$rw->kurikulum ?></option>

            <?php } ?>
                
        </select>    
		<?php

	}

	public function get_select_semester($id_prodi)
	{
		$this->db->where('id_prodi', $id_prodi);
		$tot_semester  = $this->db->get('prodi')->row()->jumlah_semester;

		?>
		<select name="semester" id="semester" style="width:100%;">
			<option value="">--Pilih Semester --</option>
			<?php 
            for ($i=1; $i <= $tot_semester ; $i++) { 
             ?>
             <option value="<?php echo $i ?>"><?php echo $i ?></option>
           	<?php } ?>
		</select>


		<?php 
	}

	
	public function set_online_user($status)
	{
		$this->db->where('id_user', $this->session->userdata('id_user'));
		$this->db->update('users', array('online'=>$status));
	}

	public function reset_password($id_user)
	{
		$password = password_hash('123456', PASSWORD_DEFAULT);
		$this->db->where('id_user', $id_user);
		$this->db->update('users', array('password'=>$password));
		$this->session->set_flashdata('notif', alert_biasa('Password berhasil direset','success'));
		redirect('users','refresh');
	}

	

	

    public function profil()
    {
    	if ($_POST) {
    		$password_lama = $this->input->post('password_lama');
    		$password_baru = $this->input->post('password_baru');

    		if ($password_baru != '') {
    			$cek_pass = get_data('users','id_user',$this->session->userdata('id_user'),'password');
    			if (password_verify($password_lama, $cek_pass)) {
    				$data['password'] = password_hash($password_baru, PASSWORD_DEFAULT);
    			} else {
    				$this->session->set_flashdata('notif', alert_biasa('password lama yang kamu masukkan tidak cocok','warning'));
					redirect('app/profil','refresh');
    			}
    		}

    		$data['foto'] = $retVal = ($_FILES['foto']['name'] == '') ? $_POST['foto_old'] : upload_gambar_biasa('user', 'image/user/', 'jpeg|png|jpg|gif', 10000, 'foto');
    		$data['wajib_ubah_password'] = "t";
    		
    		$this->db->where('id_user', $this->session->userdata('id_user'));
            $update = $this->db->update('users', $data);
            if ($update) {
                $this->session->set_flashdata('notif', alert_biasa('profil kamu berhasil diupdate','success'));
                redirect('app/profil','refresh');
            }
    	} else {
    		$this->db->where('id_user', $this->session->userdata('id_user'));
	    	$data = array(
				'konten' => 'profil',
				'judul_page' => 'Profil User',
				'data' => $this->db->get('users'),
			);
			$this->load->view('v_index',$data);
    	}
    }

    public function rekap_pengawas()
	{
		if ($this->session->userdata('level') == '') {
			redirect('login','refresh');
		}
		$data = array(
			'konten' => 'rekap_pengawas/view',
			'judul_page' => 'Rekap Pengawas',
		);
		$this->load->view('v_index',$data);
	}







}
