<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_pengawas extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Users_pengawas_model');
        $this->load->library('form_validation');
        $this->rbac->check_module_access();
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_pengawas/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_pengawas/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_pengawas/index.html';
            $config['first_url'] = base_url() . 'users_pengawas/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_pengawas_model->total_rows($q);
        $users = $this->Users_pengawas_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_data' => $users,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Daftar User Pengawas',
            'konten' => 'users_pengawas/users_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_pengawas_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_user' => $row->id_user,
		'nama' => $row->nama,
		'username' => $row->username,
		'password' => $row->password,
		'level' => $row->level,
		'foto' => $row->foto,
		'keterangan' => $row->keterangan,
		'last_login' => $row->last_login,
		'online' => $row->online,
	    );
            $this->load->view('users_pengawas/users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Tambah User Pengawas',
            'konten' => 'users_pengawas/users_form',
            'button' => 'Simpan',
            'action' => site_url('users_pengawas/create_action'),
	    'id' => $this->gen_uuid(),
	    'name' => set_value('name'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'id_bidang' => set_value('id_bidang'),
	    'id_dpa' => set_value('id_dpa'),
	    'tanggal_mulai' => set_value('tanggal_mulai'),
	    'tanggal_selesai' => set_value('tanggal_selesai'),
	    'no_hp' => set_value('no_hp'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $this->db->where('username', $this->input->post('username'));
            $cek_username = $this->db->get('users_pengawas');

            if ($cek_username->num_rows() > 0) {
                $this->session->set_flashdata('message', alert_notif('Username '.$this->input->post('username').' sudah ada !','danger'));
                redirect('users_pengawas','refresh');
            }

            $data = array(
            	'id'=> $this->input->post('id'),
		'name' => $this->input->post('name',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => password_hash($this->input->post('password',TRUE), PASSWORD_DEFAULT),
		'id_bidang' => $this->input->post('id_bidang',TRUE),
		'no_hp' => $this->input->post('no_hp',TRUE),
		'id_dpa' => $this->input->post('id_dpa',TRUE),
		'tanggal_mulai' => $retVal = ($this->input->post('tanggal_mulai',TRUE) == '') ? null : $this->input->post('tanggal_mulai',TRUE),
		'tanggal_selesai' => $retVal = ($this->input->post('tanggal_selesai',TRUE) == '') ? null : $this->input->post('tanggal_selesai',TRUE),
		'created_on' => get_waktu()
	    );

            $this->Users_pengawas_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('users_pengawas'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_pengawas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Ubah User Pengguna',
                'konten' => 'users_pengawas/users_form',
                'button' => 'Ubah',
                'action' => site_url('users_pengawas/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'id_dpa' => set_value('id_dpa', $row->id_dpa),
		'id_bidang' => set_value('id_bidang', $row->id_bidang),
		'no_hp' => set_value('no_hp', $row->no_hp),
		'tanggal_mulai' => set_value('tanggal_mulai', $row->tanggal_mulai),
		'tanggal_selesai' => set_value('tanggal_selesai', $row->tanggal_selesai),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $retVal = ($this->input->post('password') != '') ? password_hash($this->input->post('password',TRUE), PASSWORD_DEFAULT) : $this->input->post('password_old') ,
		'id_dpa' => $this->input->post('id_dpa',TRUE),
		'id_bidang' => $this->input->post('id_bidang',TRUE),
		'no_hp' => $this->input->post('no_hp',TRUE),
		'tanggal_mulai' => $retVal = ($this->input->post('tanggal_mulai',TRUE) == '') ? null : $this->input->post('tanggal_mulai',TRUE),
		'tanggal_selesai' => $retVal = ($this->input->post('tanggal_selesai',TRUE) == '') ? null : $this->input->post('tanggal_selesai',TRUE),
	    );
            

            $this->Users_pengawas_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('users_pengawas'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_pengawas_model->get_by_id($id);

        if ($row) {
            $this->Users_pengawas_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('users_pengawas'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_pengawas'));
        }
    }

    public function generate_laporan()
	{
		if ($_GET['id_dpa']) {
			$id_user = $this->input->get('id_user');
			$id_dpa = $this->input->get('id_dpa');
			$start = $this->input->get('start');
			$end = $this->input->get('end');


			//delete data sebelumnya
			$this->db->where('id_user', $id_user);
			$this->db->where('id_dpa', $id_dpa);
			$this->db->delete('laporan_pengawas');

			$this->db->where('id_user', $id_user);
			$this->db->where('id_dpa', $id_dpa);
			$this->db->delete('pekerjaan');

			//insert data pekerjaan
			$this->db->insert('pekerjaan', array(
						"id_dpa" => $id_dpa,
						"id_user" => $id_user,
						"tanggal_mulai_kontrak" => $start,
						"tanggal_akhir_kontrak" => $end,
						"created_on" => get_waktu(),
						"modified_on" => get_waktu(),
					));


			//insert data laporan

			$startDateString = $start;
			$endDateString = $end;

			$startDate = strtotime($startDateString);
			$endDate = strtotime($endDateString);

			$currentDate = $startDate;
			// $counter = 1;

			while ($currentDate <= $endDate) {
				if ($currentDate != $startDate) {
					$this->db->insert('laporan_pengawas', array(
						"jadwal_laporan" => date("Y-m-d", $currentDate),
						"id_dpa" => $id_dpa,
						"id_user" => $id_user,
						"created_on" => get_waktu(),
						"modified_on" => get_waktu(),
					));
				}
			    $nextDate = strtotime("+7 day", $currentDate);
			    $currentDate = $nextDate;
			}

			if ($currentDate > $endDate) {
			    $this->db->insert('laporan_pengawas', array(
					"jadwal_laporan" => date("Y-m-d", $endDate),
					"id_dpa" => $id_dpa,
					"id_user" => $id_user,
					"created_on" => get_waktu(),
					"modified_on" => get_waktu(),
				));
			}
			echo "sukses";

		}




	}

	public function update_bypass($id, $status)
	{
	    // Pastikan parameter valid
	    if (!in_array($status, [0, 1])) {
	        show_error('Invalid bypass status.');
	    }

	    // Update kolom bypass
	    $this->db->where('id', $id);
	    $this->db->update('users_pengawas', ['bypass' => $status]);

	    // Redirect kembali ke halaman sebelumnya
	    $this->session->set_flashdata('message', 'Status bypass berhasil diperbarui.');
	    redirect($_SERVER['HTTP_REFERER']);
	}

	public function perpanjang_jadwal()
	{
		if ($_GET['id_dpa']) {
			$id_user = $this->input->get('id_user');
			$id_dpa = $this->input->get('id_dpa');
			$start = $this->input->get('start');
			$end = $this->input->get('end');

			//insert data laporan

			$startDateString = $start;
			$endDateString = $end;

			$startDate = strtotime($startDateString);
			$endDate = strtotime($endDateString);

			$currentDate = $startDate;

			while ($currentDate <= $endDate) {
			    $jadwalLaporan = date("Y-m-d", $currentDate);
			    
			    // Periksa apakah data sudah ada
			    $exists = $this->db->where('jadwal_laporan', $jadwalLaporan)
			                       ->where('id_dpa', $id_dpa)
			                       ->where('id_user', $id_user)
			                       ->get('laporan_pengawas')
			                       ->num_rows() > 0;

			    // Jika belum ada, lakukan insert
			    if (!$exists) {
			    	if ($currentDate != $startDate) {
				        $this->db->insert('laporan_pengawas', array(
				            "jadwal_laporan" => $jadwalLaporan,
				            "id_dpa" => $id_dpa,
				            "id_user" => $id_user,
				            "created_on" => get_waktu(),
				            "modified_on" => get_waktu(),
				        ));
				    }
			    }

			    // Tambahkan 7 hari
			    $nextDate = strtotime("+7 day", $currentDate);
			    $currentDate = $nextDate;
			}

			// Pastikan tanggal akhir juga diperiksa
			if ($endDate > $startDate) {
			    $jadwalLaporan = date("Y-m-d", $endDate);

			    $exists = $this->db->where('jadwal_laporan', $jadwalLaporan)
			                       ->where('id_dpa', $id_dpa)
			                       ->where('id_user', $id_user)
			                       ->get('laporan_pengawas')
			                       ->num_rows() > 0;

			    if (!$exists) {
			        $this->db->insert('laporan_pengawas', array(
			            "jadwal_laporan" => $jadwalLaporan,
			            "id_dpa" => $id_dpa,
			            "id_user" => $id_user,
			            "created_on" => get_waktu(),
			            "modified_on" => get_waktu(),
			        ));
			    }
			}

			echo "sukses";


		}
	}

	public function lagi()
	{
		$dpa = [];
		$sql = "SELECT id_dpa FROM dpa WHERE uraian LIKE 'pembangunan%'";
		foreach ($this->db->query($sql)->result() as $key => $value) {
			$dpa[$key]  = $value->id_dpa;
		}
		//log_data($dpa);

		for ($i=2; $i <= 100; $i++) {
			//log_data($dpa[$i]);
			$this->db->where('id', $i);
			$this->db->update('users_pengawas', array("id_dpa" => $dpa[$i]));
		}

	}

	public function coba()
	{
		for ($i=2; $i <= 100; $i++) {
			$username = "user".$i; 
			$start = "2024-01-10";
			$end = "2024-03-10";
			$id_dpa = 3488;

			$user = array(
				"id" => $i,
				"name" => "User $i",
				"username" => $username,
				"password" => $username,//password_hash($username, PASSWORD_DEFAULT),
				"id_dpa" => $id_dpa,
				"tanggal_mulai" => $start,
				"tanggal_selesai" => $end,
				"created_on" => get_waktu(),
				"modified_on" => get_waktu(),
			);
			

			$this->db->insert('users_pengawas', $user);
			//insert data pekerjaan
			$this->db->insert('pekerjaan', array(
						"id_dpa" => $id_dpa,
						"id_user" => $i,
						"tanggal_mulai_kontrak" => $start,
						"tanggal_akhir_kontrak" => $end,
						"created_on" => get_waktu(),
						"modified_on" => get_waktu(),
					));


			//insert data laporan

			$startDateString = $start;
			$endDateString = $end;

			$startDate = strtotime($startDateString);
			$endDate = strtotime($endDateString);

			$currentDate = $startDate;
			// $counter = 1;

			while ($currentDate <= $endDate) {
				if ($currentDate != $startDate) {
					$this->db->insert('laporan_pengawas', array(
						"jadwal_laporan" => date("Y-m-d", $currentDate),
						"id_dpa" => $id_dpa,
						"id_user" => $i,
						"created_on" => get_waktu(),
						"modified_on" => get_waktu(),
					));
				}
			    $nextDate = strtotime("+7 day", $currentDate);
			    // if (date("m", $nextDate) == date("m", $currentDate)) {
			    //     $counter++;
			    // } else {
			    //     $counter = 1;
			    // }
			    $currentDate = $nextDate;
			}

			if ($currentDate > $endDate) {
			    $this->db->insert('laporan_pengawas', array(
					"jadwal_laporan" => date("Y-m-d", $endDate),
					"id_dpa" => $id_dpa,
					"id_user" => $i,
					"created_on" => get_waktu(),
					"modified_on" => get_waktu(),
				));
			}

		}
	}

	function gen_uuid() {
	    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
	        // 32 bits for "time_low"
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

	        // 16 bits for "time_mid"
	        mt_rand( 0, 0xffff ),

	        // 16 bits for "time_hi_and_version",
	        // four most significant bits holds version number 4
	        mt_rand( 0, 0x0fff ) | 0x4000,

	        // 16 bits, 8 bits for "clk_seq_hi_res",
	        // 8 bits for "clk_seq_low",
	        // two most significant bits holds zero and one for variant DCE1.1
	        mt_rand( 0, 0x3fff ) | 0x8000,

	        // 48 bits for "node"
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	    );
	}

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

	

}