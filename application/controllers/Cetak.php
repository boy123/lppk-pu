<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->rbac->check_module_access();
	}

	public function cetak_rekap_bidang_header()
	{
		if (get_data('setting','id_setting','1','show_dpap') == 'y') {
			$this->load->view('cetak/dpap/cetak_rekap_header');
		} else {
			$this->load->view('cetak/cetak_rekap_header');
		}
		
	}

	public function cetak_rekap_bidang_detail()
	{
		$this->load->view('cetak/cetak_rekap_header_detail');
	}

	public function cetak_rekap_dinas()
	{
		if (get_data('setting','id_setting','1','show_dpap') == 'y') {
			$this->load->view('cetak/dpap/cetak_rekap_dinas');
		} else{
			$this->load->view('cetak/cetak_rekap_dinas');
		}
		
	}

	public function cetak_rekap_perkegiatan()
	{
		if (get_data('setting','id_setting','1','show_dpap') == 'y') {
			$this->load->view('cetak/dpap/cetak_rekap_perkegiatan');
		} else {
			$this->load->view('cetak/cetak_rekap_perkegiatan');
		}
		
	}

	public function cetak_rekap_perkegiatan_khusus()
	{
		if (get_data('setting','id_setting','1','show_dpap') == 'y') {
			$this->load->view('cetak/dpap/cetak_rekap_perkegiatan_khusus');
		} else {
			$this->load->view('cetak/cetak_rekap_perkegiatan_khusus');
		}
		
	}

	public function cetak_rekap_pengawas_by_user()
	{
		$this->load->view('cetak/cetak_rekap_pengawas_by_user');
	}

	public function cetak_rekap_pengawas_by_bidang()
	{
		$this->load->view('cetak/cetak_rekap_pengawas_by_bidang');
	}
	public function cetak_progress_pengawas_by_bidang()
	{
		$this->load->view('cetak/cetak_progress_pengawas_by_bidang');
	}

	public function search_kegiatan()
	{
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');
		$id_bidang = $this->input->get('id_bidang');
		$id_subkegiatan = $this->input->get('id_subkegiatan');

		$this->db->where('id_bidang', $id_bidang);
		$this->db->where('bulan', $bulan);
		$this->db->where('tahun', $tahun);
		$this->db->where('id_subkegiatan', $id_subkegiatan);
		$pptk = $this->db->get('rekap_header')->row()->created_user;

		$kegiatan = get_data('subkegiatan','id_subkegiatan',$id_subkegiatan, 'id_kegiatan');
		$url = "?bulan=".$bulan."&tahun=".$tahun."&id_bidang=".$id_bidang."&pptk=".$pptk."&kegiatan=".$kegiatan;
		?>
		<meta content="0; url=<?php echo base_url() ?>cetak/cetak_rekap_perkegiatan<?php echo $url ?>" http-equiv="refresh">
		<?php
		//redirect(base_url().'cetak/cetak_rekap_perkegiatan'+$url,'refresh');
	}


}
