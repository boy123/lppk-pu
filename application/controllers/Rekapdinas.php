<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapdinas extends CI_Controller {

	public function index()
	{
		$this->rbac->check_operation_access();
		$data = array(
			'konten' => 'rekap_dinas/view',
			'judul_page' => 'Cetak Rekap Dinas',
		);
		$this->load->view('v_index',$data);
	}

}

/* End of file Rekapdinas.php */
/* Location: ./application/controllers/Rekapdinas.php */