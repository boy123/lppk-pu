<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
        $this->load->model('Users_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pegawai/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pegawai/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pegawai/index.html';
            $config['first_url'] = base_url() . 'pegawai/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pegawai_model->total_rows($q);
        $pegawai = $this->Pegawai_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pegawai_data' => $pegawai,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Pegawai',
            'konten' => 'pegawai/pegawai_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Pegawai_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pegawai' => $row->id_pegawai,
		'nip' => $row->nip,
		'nama' => $row->nama,
		'id_jabatan' => $row->id_jabatan,
		'id_bidang' => $row->id_bidang,
	    );
            $this->load->view('pegawai/pegawai_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pegawai'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Pegawai',
            'konten' => 'pegawai/pegawai_form',
            'button' => 'Simpan',
            'action' => site_url('pegawai/create_action'),
	    'id_pegawai' => set_value('id_pegawai'),
	    'nip' => set_value('nip'),
	    'nama' => set_value('nama'),
	    'id_jabatan' => set_value('id_jabatan'),
	    'id_bidang' => set_value('id_bidang'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'id_jabatan' => $this->input->post('id_jabatan',TRUE),
		'id_bidang' => $this->input->post('id_bidang',TRUE),
	    );

            $this->Pegawai_model->insert($data);

            //insert data user
            $data_user = array(
            'nama' => $this->input->post('nama',TRUE),
            'username' => $this->input->post('nip',TRUE),
            'password' => password_hash("123456", PASSWORD_DEFAULT),
            'level' => 7,
            'foto' => "default.png",
            'keterangan' => $this->db->insert_id(),
            );

                $this->Users_model->insert($data_user);

            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan & data akun login berhasil dibuat !
                                    </div>');
            redirect(site_url('pegawai'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Pegawai',
                'konten' => 'pegawai/pegawai_form',
                'button' => 'Ubah',
                'action' => site_url('pegawai/update_action'),
		'id_pegawai' => set_value('id_pegawai', $row->id_pegawai),
		'nip' => set_value('nip', $row->nip),
		'nama' => set_value('nama', $row->nama),
		'id_jabatan' => set_value('id_jabatan', $row->id_jabatan),
		'id_bidang' => set_value('id_bidang', $row->id_bidang),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pegawai'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pegawai', TRUE));
        } else {
            $data = array(
		'nip' => $this->input->post('nip',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'id_jabatan' => $this->input->post('id_jabatan',TRUE),
		'id_bidang' => $this->input->post('id_bidang',TRUE),
	    );

            $this->Pegawai_model->update($this->input->post('id_pegawai', TRUE), $data);

            //update data user
            $data_user = array(
            'nama' => $this->input->post('nama',TRUE),
            'username' => $this->input->post('nip',TRUE),
            'password' => password_hash("123456", PASSWORD_DEFAULT)
            );

            $this->db->where('keterangan', $this->input->post('id_pegawai', TRUE));
            $this->db->update('users', $data_user);

            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan & data akun login berhasil dibuat !
                                    </div>');

            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('pegawai'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pegawai_model->get_by_id($id);

        if ($row) {
            $this->Pegawai_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('pegawai'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pegawai'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('id_jabatan', 'id jabatan', 'trim|required');
	$this->form_validation->set_rules('id_bidang', 'id bidang', 'trim|required');

	$this->form_validation->set_rules('id_pegawai', 'id_pegawai', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-11-09 04:50:13 */
/* https://jualkoding.com */