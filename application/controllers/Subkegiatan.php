<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subkegiatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Subkegiatan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'subkegiatan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'subkegiatan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'subkegiatan/index.html';
            $config['first_url'] = base_url() . 'subkegiatan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Subkegiatan_model->total_rows($q);
        $subkegiatan = $this->Subkegiatan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'subkegiatan_data' => $subkegiatan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Subkegiatan',
            'konten' => 'subkegiatan/subkegiatan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Subkegiatan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_subkegiatan' => $row->id_subkegiatan,
		'id_kegiatan' => $row->id_kegiatan,
		'kode_rekening' => $row->kode_rekening,
		'subkegiatan' => $row->subkegiatan,
	    );
            $this->load->view('subkegiatan/subkegiatan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subkegiatan'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Subkegiatan',
            'konten' => 'subkegiatan/subkegiatan_form',
            'button' => 'Simpan',
            'action' => site_url('subkegiatan/create_action'),
	    'id_subkegiatan' => set_value('id_subkegiatan'),
	    'id_kegiatan' => set_value('id_kegiatan'),
	    'kode_rekening' => set_value('kode_rekening'),
	    'subkegiatan' => set_value('subkegiatan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_kegiatan' => $this->input->post('id_kegiatan',TRUE),
		'kode_rekening' => $this->input->post('kode_rekening',TRUE),
		'subkegiatan' => $this->input->post('subkegiatan',TRUE),
	    );

            $this->Subkegiatan_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('subkegiatan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Subkegiatan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Subkegiatan',
                'konten' => 'subkegiatan/subkegiatan_form',
                'button' => 'Ubah',
                'action' => site_url('subkegiatan/update_action'),
		'id_subkegiatan' => set_value('id_subkegiatan', $row->id_subkegiatan),
		'id_kegiatan' => set_value('id_kegiatan', $row->id_kegiatan),
		'kode_rekening' => set_value('kode_rekening', $row->kode_rekening),
		'subkegiatan' => set_value('subkegiatan', $row->subkegiatan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subkegiatan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_subkegiatan', TRUE));
        } else {
            $data = array(
		'id_kegiatan' => $this->input->post('id_kegiatan',TRUE),
		'kode_rekening' => $this->input->post('kode_rekening',TRUE),
		'subkegiatan' => $this->input->post('subkegiatan',TRUE),
	    );

            $this->Subkegiatan_model->update($this->input->post('id_subkegiatan', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('subkegiatan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Subkegiatan_model->get_by_id($id);

        if ($row) {
            $this->Subkegiatan_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('subkegiatan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subkegiatan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_kegiatan', 'id kegiatan', 'trim|required');
	$this->form_validation->set_rules('kode_rekening', 'kode rekening', 'trim|required');
	$this->form_validation->set_rules('subkegiatan', 'subkegiatan', 'trim|required');

	$this->form_validation->set_rules('id_subkegiatan', 'id_subkegiatan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Subkegiatan.php */
/* Location: ./application/controllers/Subkegiatan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-10-14 14:55:35 */
/* https://jualkoding.com */