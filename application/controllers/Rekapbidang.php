<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapbidang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		check_buka_aplikasi();
	}

	public function index()
	{
		$this->rbac->check_operation_access();
		$data = array(
			'konten' => 'rekap_bidang/view',
			'judul_page' => 'Data Bidang',
		);
		$this->load->view('v_index',$data);
	}

	public function create()
	{
		$data = array(
			'konten' => 'rekap_bidang/add_header',
			'judul_page' => 'Tambah Rekap Bidang Header',
		);
		$this->load->view('v_index',$data);
	}

	public function detail($id_rekap_header)
	{
		$data = array(
			'konten' => 'rekap_bidang/view_detail',
			'judul_page' => 'Tambah Rekap Bidang',
			'data_header' => $this->db->get_where('rekap_header', array('id_rekap_header'=>$id_rekap_header)),
		);
		$this->load->view('v_index',$data);
	}

	public function input_detail($id_rekap_header)
	{
		$data = array(
			'konten' => 'rekap_bidang/input_detail',
			'judul_page' => 'Tambah Rekap',
			'data_header' => $this->db->get_where('rekap_header', array('id_rekap_header'=>$id_rekap_header)),
		);
		$this->load->view('v_index',$data);
	}

	public function edit_detail($id_rekap_detail)
	{
		$data = array(
			'konten' => 'rekap_bidang/edit_detail',
			'judul_page' => 'Edit Rekap',
			'data_detail' => $this->db->get_where('rekap_detail', array('id_rekap_detail'=>$id_rekap_detail)),
		);
		$this->load->view('v_index',$data);
	}

	public function save_rekap_detail($id_rekap_header)
	{
		$this->db->trans_begin();

		$id_dpa = $this->input->post('id_dpa');
		$dana_dpa = dana_dpa($id_dpa);
		$volume = $retVal = ($this->input->post('volume') == '') ? NULL : $this->input->post('volume');
		$nilai_kontrak = preg_replace("/[^0-9,]/", "", $retVal = ($this->input->post('nilai_kontrak') == '') ? 0 : $this->input->post('nilai_kontrak'));
		$nilai_kontrak = str_replace(',','.',$nilai_kontrak);

		$target = $this->input->post('target');
		$realisasi = $this->input->post('realisasi');
		$nilai_penyerapan = preg_replace("/[^0-9,]/", "", $this->input->post('nilai_penyerapan'));
		$nilai_penyerapan = str_replace(',','.',$nilai_penyerapan);

		// mencari deviasi
		$deviasi = $realisasi - $target;

		//mencari persentase penyerapan
		$persen_penyerapan = $nilai_penyerapan/$dana_dpa * 100;

		//mencari sisa kontrak
		$sisa_kontrak = 0;
		if (!empty($nilai_kontrak)) {
			$sisa_kontrak = $nilai_kontrak - $nilai_penyerapan;	
		}

		//mencari sisa anggaran
		$sisa_anggaran = $dana_dpa - $nilai_penyerapan;


		$data = array(
			'id_rekap_header' => $id_rekap_header,
			'id_dpa' => $this->input->post('id_dpa'),
			'volume' => $retVal = ($this->input->post('volume') == '') ? NULL : $this->input->post('volume'),
			'lokasi' => $this->input->post('lokasi'),
			'type' => $this->input->post('type'),
			'penyedia_jasa' => $this->input->post('penyedia_jasa'),
			'nilai_kontrak' => $retVal = ($this->input->post('nilai_kontrak') == '') ? NULL : $nilai_kontrak,
			'mulai_kontrak' => $retVal = ($this->input->post('mulai_kontrak') == '') ? NULL : $this->input->post('mulai_kontrak'),
			'selesai_kontrak' => $retVal = ($this->input->post('selesai_kontrak') == '') ? NULL : $this->input->post('selesai_kontrak'),
			'target' => $this->input->post('target'),
			'realisasi' => $this->input->post('realisasi'),
			'deviasi' => $deviasi,
			'nilai_penyerapan' => $nilai_penyerapan,
			'persen_penyerapan' => $persen_penyerapan,
			'sisa_kontrak' => $sisa_kontrak,
			'sisa_anggaran' => $sisa_anggaran,
			'ket' => $this->input->post('ket'),
			'foto' => $retVal = ($_FILES['foto']['name'] == '') ? NULL : upload_gambar_biasa('kegiatan', 'image/kegiatan/', 'jpeg|png|jpg|gif', 10000, 'foto'),
			'created_user' => $this->session->userdata('id_user'),
			'created_at' => get_waktu(),
		);
		
        $this->db->insert('rekap_detail', $data);


        saveLog('Menginput Rekap bidang', $this->session->userdata('id_user'));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data Rekap gagal disimpan !','warning'));
            redirect('rekapbidang/detail/'.$id_rekap_header.'?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();

            $id_dpa_parent = get_data('dpa','id_dpa',$id_dpa,'parent');
			if ($id_dpa_parent != '' AND $id_dpa_parent > 0) {

				//check subparent yang sudah di input
				$sql_data_detail = "SELECT
										sum(a.volume) as volume, sum(a.target) as target, sum(a.realisasi) as realisasi, sum(a.nilai_penyerapan) as nilai_penyerapan, created_user
									FROM
										rekap_detail as a
										INNER JOIN dpa as b ON a.id_dpa=b.id_dpa
									WHERE
										a.id_rekap_header = $id_rekap_header
										and b.parent = $id_dpa_parent";

				$data_rekap_detail = $this->db->query($sql_data_detail)->row();

				//check id_dpa_parent di table temp
				$this->db->where('id_dpa', $id_dpa_parent);
				$this->db->where('id_rekap_header', $id_rekap_header);
				$rdt = $this->db->get('rekap_detail_temp');
				if ($rdt->num_rows() > 0) {
					// lakukan update

					$dt_update = array(
						"volume" => $data_rekap_detail->volume,
						"target" => $data_rekap_detail->target,
						"realisasi" => $data_rekap_detail->realisasi,
						"nilai_penyerapan" => $data_rekap_detail->nilai_penyerapan,
						"updated_at" => get_waktu(),
						"updated_user" => $data_rekap_detail->created_user
					);
					$this->db->where('id_dpa', $id_dpa_parent);
					$this->db->where('id_rekap_header', $id_rekap_header);
					$this->db->update('rekap_detail_temp', $dt_update);

				} else {
					//lakukan insert
					$dt_insert = array(
						"id_rekap_header" => $id_rekap_header,
						"id_dpa" => $id_dpa_parent,
						"volume" => $data_rekap_detail->volume,
						"target" => $data_rekap_detail->target,
						"realisasi" => $data_rekap_detail->realisasi,
						"nilai_penyerapan" => $data_rekap_detail->nilai_penyerapan,
						"created_at" => get_waktu(),
						"created_user" => $data_rekap_detail->created_user
					);
					$this->db->insert('rekap_detail_temp', $dt_insert);
				}

				?>
				<script type="text/javascript">
					alert("sukses perbaiki parent rekap !");
				</script>
				<?php

			} else {

				?>
				<script type="text/javascript">
					alert("tidak ada data parent !");
				</script>

				<?php
			}
            $this->session->set_flashdata('message', alert_notif('Data Rekap berhasil disimpan !','success'));
            redirect('rekapbidang/detail/'.$id_rekap_header.'?'.param_get(),'refresh');
        }
	}

	public function update_rekap_detail($id_rekap_detail, $id_rekap_header)
	{
		$this->db->trans_begin();

		$id_dpa = $this->input->post('id_dpa');
		$dana_dpa = dana_dpa($id_dpa);
		$volume = $retVal = ($this->input->post('volume') == '') ? NULL : $this->input->post('volume');
		$nilai_kontrak = preg_replace("/[^0-9,]/", "", $retVal = ($this->input->post('nilai_kontrak') == '') ? 0 : $this->input->post('nilai_kontrak'));
		$nilai_kontrak = str_replace(',','.',$nilai_kontrak);
		$target = $this->input->post('target');
		$realisasi = $this->input->post('realisasi');
		$nilai_penyerapan = preg_replace("/[^0-9,]/", "", $this->input->post('nilai_penyerapan'));
		$nilai_penyerapan = str_replace(',','.',$nilai_penyerapan);

		// mencari deviasi
		$deviasi = $realisasi - $target;

		//mencari persentase penyerapan
		$persen_penyerapan = $nilai_penyerapan/$dana_dpa * 100;

		//mencari sisa kontrak
		$sisa_kontrak = 0;
		if (!empty($nilai_kontrak)) {
			$sisa_kontrak = $nilai_kontrak - $nilai_penyerapan;	
		}

		//mencari sisa anggaran
		$sisa_anggaran = $dana_dpa - $nilai_penyerapan;




		$data = array(
			'id_rekap_header' => $id_rekap_header,
			'id_dpa' => $this->input->post('id_dpa'),
			'volume' => $retVal = ($this->input->post('volume') == '') ? NULL : $this->input->post('volume'),
			'lokasi' => $this->input->post('lokasi'),
			'type' => $this->input->post('type'),
			'penyedia_jasa' => $this->input->post('penyedia_jasa'),
			'nilai_kontrak' => $retVal = ($this->input->post('nilai_kontrak') == '') ? NULL : $nilai_kontrak,
			'mulai_kontrak' => $retVal = ($this->input->post('mulai_kontrak') == '') ? NULL : $this->input->post('mulai_kontrak'),
			'selesai_kontrak' => $retVal = ($this->input->post('selesai_kontrak') == '') ? NULL : $this->input->post('selesai_kontrak'),
			'target' => $this->input->post('target'),
			'realisasi' => $this->input->post('realisasi'),
			'deviasi' => $deviasi,
			'nilai_penyerapan' => $nilai_penyerapan,
			'persen_penyerapan' => $persen_penyerapan,
			'sisa_kontrak' => $sisa_kontrak,
			'sisa_anggaran' => $sisa_anggaran,
			'ket' => $this->input->post('ket'),
			'foto' => $retVal = ($_FILES['foto']['name'] == '') ? $_POST['foto_old'] : upload_gambar_biasa('kegiatan', 'image/kegiatan/', 'jpeg|png|jpg|gif', 10000, 'foto'),
			'updated_user' => $this->session->userdata('id_user'),
			'updated_at' => get_waktu(),
		);
		// log_data($dana_dpa);
		// log_r($data);

		
		$this->db->where('id_rekap_detail', $id_rekap_detail);
        $this->db->update('rekap_detail', $data);

        saveLog('Mengupdate Rekap bidang', $this->session->userdata('id_user'));

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data Rekap gagal diubah !','warning'));
            redirect('rekapbidang/detail/'.$id_rekap_header.'?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $id_dpa_parent = get_data('dpa','id_dpa',$id_dpa,'parent');
			if ($id_dpa_parent != '' AND $id_dpa_parent > 0) {

				//check subparent yang sudah di input
				$sql_data_detail = "SELECT
										sum(a.volume) as volume, sum(a.target) as target, sum(a.realisasi) as realisasi, sum(a.nilai_penyerapan) as nilai_penyerapan, created_user
									FROM
										rekap_detail as a
										INNER JOIN dpa as b ON a.id_dpa=b.id_dpa
									WHERE
										a.id_rekap_header = $id_rekap_header
										and b.parent = $id_dpa_parent";

				$data_rekap_detail = $this->db->query($sql_data_detail)->row();

				//check id_dpa_parent di table temp
				$this->db->where('id_dpa', $id_dpa_parent);
				$this->db->where('id_rekap_header', $id_rekap_header);
				$rdt = $this->db->get('rekap_detail_temp');
				if ($rdt->num_rows() > 0) {
					// lakukan update

					$dt_update = array(
						"volume" => $data_rekap_detail->volume,
						"target" => $data_rekap_detail->target,
						"realisasi" => $data_rekap_detail->realisasi,
						"nilai_penyerapan" => $data_rekap_detail->nilai_penyerapan,
						"updated_at" => get_waktu(),
						"updated_user" => $data_rekap_detail->created_user
					);
					$this->db->where('id_dpa', $id_dpa_parent);
					$this->db->where('id_rekap_header', $id_rekap_header);
					$this->db->update('rekap_detail_temp', $dt_update);

				} else {
					//lakukan insert
					$dt_insert = array(
						"id_rekap_header" => $id_rekap_header,
						"id_dpa" => $id_dpa_parent,
						"volume" => $data_rekap_detail->volume,
						"target" => $data_rekap_detail->target,
						"realisasi" => $data_rekap_detail->realisasi,
						"nilai_penyerapan" => $data_rekap_detail->nilai_penyerapan,
						"created_at" => get_waktu(),
						"created_user" => $data_rekap_detail->created_user
					);
					$this->db->insert('rekap_detail_temp', $dt_insert);
				}

				?>
				<script type="text/javascript">
					alert("sukses perbaiki parent rekap !");
				</script>
				<?php

			} else {

				?>
				<script type="text/javascript">
					alert("tidak ada data parent !");
				</script>

				<?php
			}
            $this->session->set_flashdata('message', alert_notif('Data Rekap berhasil diubah !','success'));
            redirect('rekapbidang/detail/'.$id_rekap_header.'?'.param_get(),'refresh');
        }
	}

	public function create_action()
	{
		
		$this->db->trans_begin();
		$data = array(
			'pptk' => $this->input->post('pptk'),
			'id_subkegiatan' => $this->input->post('id_subkegiatan'),
			'id_bidang' => $this->input->get('id_bidang'),
			'bulan' => $this->input->get('bulan'),
			'tahun' => $this->input->get('tahun'),
			'created_user' => $this->session->userdata('id_user'),
			'created_at' => get_waktu(),
		);
        $this->db->insert('rekap_header', $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data Rekap gagal disimpan !','warning'));
            redirect('rekapbidang?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Data Rekap berhasil disimpan !','success'));
            redirect('rekapbidang?'.param_get(),'refresh');
        }

	}

	public function clone_dari_bulan_lalu()
	{
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');
		$id_bidang = $this->input->get('id_bidang');

		$this->db->trans_begin();
		$bulan_old = $bulan - 1;
		$this->db->where('bulan', $bulan_old);
		$this->db->where('tahun', $tahun);
		$this->db->where('id_bidang', $id_bidang);
		// $this->db->where('created_user', $this->session->userdata('id_user'));
		$a = $this->db->get('rekap_header');
		if ($a->num_rows() == 0) {
			$this->session->set_flashdata('message', alert_notif('Data Rekap di bulan lalu tidak ditemukan !','danger'));
            redirect('rekapbidang?'.param_get(),'refresh');
            exit;
		}
		foreach ($a->result() as $header) {
			//insert clone header
			$data_header = array(
				"pptk" => $header->pptk,
				"id_subkegiatan" => $header->id_subkegiatan,
				"id_bidang" => $header->id_bidang,
				"bulan" => $bulan,
				"tahun" => $tahun,
				"created_at" => get_waktu(),
				"created_user" => $header->created_user,
			);
			$this->db->insert('rekap_header', $data_header);
			$id_rekap_header = $this->db->insert_id();

			$this->db->where('id_rekap_header', $header->id_rekap_header);
			foreach ($this->db->get('rekap_detail')->result() as $detail) {
				$data_detail = array(
					"id_rekap_header" => $id_rekap_header,
					"id_dpa" => $detail->id_dpa,
					"uraian" => $detail->uraian,
					"volume" => $detail->volume,
					"lokasi" => $detail->lokasi,
					"type" => $detail->type,
					"penyedia_jasa" => $detail->penyedia_jasa,
					"nilai_kontrak" => $detail->nilai_kontrak,
					"mulai_kontrak" => $detail->mulai_kontrak,
					"selesai_kontrak" => $detail->selesai_kontrak,
					"target" => $detail->target,
					"realisasi" => $detail->realisasi,
					"deviasi" => $detail->deviasi,
					"nilai_penyerapan" => $detail->nilai_penyerapan,
					"persen_penyerapan" => $detail->persen_penyerapan,
					"sisa_kontrak" => $detail->sisa_kontrak,
					"sisa_anggaran" => $detail->sisa_anggaran,
					"ket" => $detail->ket,
					"created_at" => get_waktu(),
					"created_user" => $detail->created_user,
				);
				$this->db->insert('rekap_detail', $data_detail);
			}
		}
		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Cloning Data Rekap gagal disimpan !','warning'));
            redirect('rekapbidang?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Cloning Data Rekap berhasil disimpan !','success'));
            redirect('rekapbidang?'.param_get(),'refresh');
        }
	}

	public function update($id)
	{
		$data = array(
			'konten' => 'rekap_bidang/edit_header',
			'judul_page' => 'Edit Rekap Bidang Header',
			'data' => $this->db->get_where('rekap_header', array('id_rekap_header'=>$id)),
		);
		$this->load->view('v_index',$data);
	}

	public function update_action($id)
	{
		$_POST['updated_user'] = $this->session->userdata('id_user');
		$_POST['updated_at'] = get_waktu();
		$this->db->where('id_rekap_header', $id);
		$update = $this->db->update('rekap_header', $_POST);
		if ($update) {
			$this->session->set_flashdata('message', alert_notif('Data Rekap Header berhasil diubah ','success'));
            redirect('rekapbidang?'.param_get(),'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id_rekap_header', $id);
        $this->db->delete('rekap_header');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data Rekap Header gagal dihapus ','warning'));
            redirect('rekapbidang?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->db->where('id_rekap_header', $id);
            $this->db->delete('rekap_detail');
            $this->session->set_flashdata('message', alert_notif('Data Rekap Header berhasil dihapus ','danger'));
            redirect('rekapbidang?'.param_get(),'refresh');
        }
	}

	public function delete_detail($id, $id_rekap_header)
	{
		$this->db->where('id_rekap_detail', $id);
        $this->db->delete('rekap_detail');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data Rekap Detail gagal dihapus ','warning'));
            redirect('rekapbidang/detail/'.$id_rekap_header.'?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Data Rekap Detail berhasil dihapus ','danger'));
            redirect('rekapbidang/detail/'.$id_rekap_header.'?'.param_get(),'refresh');
        }
	}

	public function hitung_parent_from_subparent($id_rekap_header)
	{
		//mencari id_dpa yang memiliki parent
		$sql = "
			SELECT
				a.id_rekap_header, b.id_dpa, b.parent
			FROM
				rekap_detail as a
				INNER JOIN dpa as b ON a.id_dpa=b.id_dpa
			WHERE
				a.id_rekap_header = $id_rekap_header
				AND b.parent > 0
			GROUP BY b.parent
		";
		$dpa_have_parent = $this->db->query($sql);

	}

	public function perbaiki_parent_dpa_direkap($id_rekap_header, $id_dpa)
	{
		$id_dpa_parent = get_data('dpa','id_dpa',$id_dpa,'parent');
		if ($id_dpa_parent != '' AND $id_dpa_parent > 0) {

			//check subparent yang sudah di input
			$sql_data_detail = "SELECT
									sum(a.volume) as volume, sum(a.target) as target, sum(a.realisasi) as realisasi, sum(a.nilai_penyerapan) as nilai_penyerapan, created_user
								FROM
									rekap_detail as a
									INNER JOIN dpa as b ON a.id_dpa=b.id_dpa
								WHERE
									a.id_rekap_header = $id_rekap_header
									and b.parent = $id_dpa_parent";

			$data_rekap_detail = $this->db->query($sql_data_detail)->row();

			//check id_dpa_parent di table temp
			$this->db->where('id_dpa', $id_dpa_parent);
			$this->db->where('id_rekap_header', $id_rekap_header);
			$rdt = $this->db->get('rekap_detail_temp');
			if ($rdt->num_rows() > 0) {
				// lakukan update

				$dt_update = array(
					"volume" => $data_rekap_detail->volume,
					"target" => $data_rekap_detail->target,
					"realisasi" => $data_rekap_detail->realisasi,
					"nilai_penyerapan" => $data_rekap_detail->nilai_penyerapan,
					"updated_at" => get_waktu(),
					"updated_user" => $data_rekap_detail->created_user
				);
				$this->db->where('id_dpa', $id_dpa_parent);
				$this->db->where('id_rekap_header', $id_rekap_header);
				$this->db->update('rekap_detail_temp', $dt_update);

			} else {
				//lakukan insert
				$dt_insert = array(
					"id_rekap_header" => $id_rekap_header,
					"id_dpa" => $id_dpa_parent,
					"volume" => $data_rekap_detail->volume,
					"target" => $data_rekap_detail->target,
					"realisasi" => $data_rekap_detail->realisasi,
					"nilai_penyerapan" => $data_rekap_detail->nilai_penyerapan,
					"created_at" => get_waktu(),
					"created_user" => $data_rekap_detail->created_user
				);
				$this->db->insert('rekap_detail_temp', $dt_insert);
			}

			?>
			<script type="text/javascript">
				alert("sukses perbaiki parent rekap !");
				history.go(-1);
			</script>
			<?php

		} else {

			?>
			<script type="text/javascript">
				alert("tidak ada data parent !");
				history.go(-1);
			</script>
			<?php
		}



	}




}

/* End of file Rekapbidang.php */
/* Location: ./application/controllers/Rekapbidang.php */