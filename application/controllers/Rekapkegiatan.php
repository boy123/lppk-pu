<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapkegiatan extends CI_Controller {

	public function index()
	{
		$this->rbac->check_operation_access();
		$data = array(
			'konten' => 'rekap_kegiatan/view',
			'judul_page' => 'Cetak Rekap Perkegiatan',
		);
		$this->load->view('v_index',$data);
	}

}

/* End of file Rekapkegiatan.php */
/* Location: ./application/controllers/Rekapkegiatan.php */