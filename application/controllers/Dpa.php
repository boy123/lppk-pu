<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dpa extends CI_Controller {

	public function index()
	{
		$this->rbac->check_operation_access();
		$data = array(
			'konten' => 'dpa/view',
			'judul_page' => 'Data DPA',
		);
		$this->load->view('v_index',$data);
	}

	public function create()
	{
		$data = array(
			'konten' => 'dpa/add',
			'judul_page' => 'Tambah DPA',
		);
		$this->load->view('v_index',$data);
	}

	public function create_action()
	{
		
		$this->db->trans_begin();

		// 1. Hapus semua titik (.) yang digunakan sebagai pemisah ribuan
		$step1 = preg_replace("/\./", "", $this->input->post('jumlah'));

		// 2. Ganti koma (,) menjadi titik (.) untuk desimal
		$final = str_replace(",", ".", $step1);


		$_POST['jumlah'] = $final;
		$_POST['parent'] = null;

        $this->db->insert('dpa', $_POST);
        saveLog('Menginput Rekap bidang', $this->session->userdata('id_user'));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data DPA gagal disimpan !','warning'));
            redirect('dpa?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Data DPA berhasil disimpan !','success'));
            redirect('dpa?'.param_get(),'refresh');
        }

	}

	public function update($id)
	{
		$data = array(
			'konten' => 'dpa/update',
			'judul_page' => 'Update DPA',
			'data' => $this->db->get_where('dpa', array('id_dpa'=>$id)),
		);
		$this->load->view('v_index',$data);
	}

	public function update_action($id)
	{
		$this->db->where('id_dpa', $id);

		// 1. Hapus semua titik (.) yang digunakan sebagai pemisah ribuan
		$step1 = preg_replace("/\./", "", $this->input->post('jumlah'));

		// 2. Ganti koma (,) menjadi titik (.) untuk desimal
		$final = str_replace(",", ".", $step1);


		$_POST['jumlah'] = $final;

		$update = $this->db->update('dpa', $_POST);
		if ($update) {
			saveLog('Mengupdate DPA', $this->session->userdata('id_user'));
			$this->session->set_flashdata('notif', alert_notif('data dpa berhasil diupdate','success'));
			redirect('dpa?'.param_get(),'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id_dpa', $id);
        $this->db->delete('dpa');

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Data DPA gagal dihapus ','warning'));
            redirect('dpa?'.param_get(),'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Data DPA berhasil dihapus ','danger'));
            redirect('dpa?'.param_get(),'refresh');
        }
	}

	public function getListKegiatan($id_bidang, $tahun)
	{
		?>
		<select name="kegiatan" id="kegiatan" style="width:100%;">
                <option value="">--Semua Kegiatan --</option>

		<?php
			$this->db->where('id_bidang', $id_bidang);
			$this->db->where('tahun', $tahun);
			foreach ($this->db->get('kegiatan')->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_kegiatan ?>"><?php echo '['.$rw->kode_rekening.'] '.$rw->kegiatan ?></option>

            <?php } ?>
                
        </select>    
		<?php
	}	


	public function getListSubkegiatan($id_kegiatan)
	{
		?>
		<select name="sub_kegiatan" id="sub_kegiatan" style="width:100%;">
			                <option value="">--Pilih Sub Kegiatan --</option>
		<?php
			$this->db->where('id_kegiatan', $id_kegiatan);
			foreach ($this->db->get('subkegiatan')->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_subkegiatan ?>"><?php echo '['.$rw->kode_rekening.'] '.$rw->subkegiatan ?></option>

            <?php } ?>
                
        </select>    
		<?php
	}	

	public function get_pptk($id_bidang)
	{
		?>
		<select name="pptk" id="pptk"  style="width:100%;">
                <option value="">--Pilih PPTK --</option>

		<?php
			$where = "";
            if ($this->session->userdata('level') == '7') {
            	$id_pegawai = $this->session->userdata('keterangan');
            	$where = "AND b.keterangan='$id_pegawai' ";
            }
            $sql = "SELECT b.id_user, a.nama FROM pegawai as a INNER JOIN users as b ON a.id_pegawai=b.keterangan WHERE a.id_jabatan=1 and id_bidang=$id_bidang $where";
			foreach ($this->db->query($sql)->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_user ?>"><?php echo $rw->nama ?></option>

            <?php } ?>
                
        </select>    
		<?php
	}	

	public function get_kegiatan_by_pptk($id_user)
	{
		?>
		<select name="kegiatan" id="kegiatan" style="width:100%;">
                <option value="">--Semua Kegiatan --</option>

		<?php
			$tahun = date('Y');
			$sql = "SELECT
						c.*
					FROM
						rekap_header as a
						INNER JOIN subkegiatan as b ON a.id_subkegiatan = b.id_subkegiatan
						INNER JOIN kegiatan as c ON b.id_kegiatan = c.id_kegiatan
					WHERE
						a.created_user = $id_user
						AND c.tahun = $tahun
					GROUP BY c.id_kegiatan";
			foreach ($this->db->query($sql)->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_kegiatan ?>"><?php echo '['.$rw->kode_rekening.'] '.$rw->kegiatan ?></option>

            <?php } ?>
                
        </select>    
		<?php
	}	

	public function get_uraian_by_bidang($bidang, $bulan, $tahun)
	{
		
		?>
		<select name="id_subkegiatan" id="kegiatan_search" style="width:100%;">
                <option value="">--Pencarian --</option>

		<?php
			$sql = "SELECT
					c.id_subkegiatan, c.uraian
				FROM
					rekap_header as a
					INNER JOIN rekap_detail as b ON a.id_rekap_header=b.id_rekap_header
					INNER JOIN dpa as c ON b.id_dpa = c.id_dpa
				WHERE a.id_bidang = $bidang and a.bulan = $bulan and a.tahun = $tahun";
			foreach ($this->db->query($sql)->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_subkegiatan ?>"><?php echo $rw->uraian ?></option>

            <?php } ?>
                
        </select>    
		<?php
	}

	public function get_uraian_by_pengawas($tahun)
	{
		
		?>
		<select name="id_dpa" id="dpa_search" style="width:100%;">
                <option value="">--Pencarian --</option>

		<?php
			$sql = "SELECT a.id_dpa, a.uraian 

                FROM dpa as a INNER JOIN users_pengawas as b ON a.id_dpa = b.id_dpa
                WHERE b.created_on like '$tahun%' ";
			foreach ($this->db->query($sql)->result() as $rw) {
			?>
			
                <option value="<?php echo $rw->id_dpa ?>"><?php echo $rw->uraian ?></option>

            <?php } ?>
                
        </select>    
		<?php
	}

}

/* End of file Dpa.php */
/* Location: ./application/controllers/Dpa.php */