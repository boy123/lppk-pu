<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kegiatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kegiatan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'kegiatan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kegiatan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'kegiatan/index.html';
            $config['first_url'] = base_url() . 'kegiatan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Kegiatan_model->total_rows($q);
        $kegiatan = $this->Kegiatan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'kegiatan_data' => $kegiatan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Kegiatan',
            'konten' => 'kegiatan/kegiatan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Kegiatan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_kegiatan' => $row->id_kegiatan,
		'kode_rekening' => $row->kode_rekening,
		'kegiatan' => $row->kegiatan,
		'id_bidang' => $row->id_bidang,
		'tahun' => $row->tahun,
	    );
            $this->load->view('kegiatan/kegiatan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kegiatan'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Kegiatan',
            'konten' => 'kegiatan/kegiatan_form',
            'button' => 'Simpan',
            'action' => site_url('kegiatan/create_action'),
	    'id_kegiatan' => set_value('id_kegiatan'),
	    'kode_rekening' => set_value('kode_rekening'),
	    'kegiatan' => set_value('kegiatan'),
	    'id_bidang' => set_value('id_bidang'),
	    'tahun' => set_value('tahun'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_rekening' => $this->input->post('kode_rekening',TRUE),
		'kegiatan' => $this->input->post('kegiatan',TRUE),
		'id_bidang' => $this->input->post('id_bidang',TRUE),
		'tahun' => $this->input->post('tahun',TRUE),
	    );

            $this->Kegiatan_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('kegiatan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Kegiatan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Kegiatan',
                'konten' => 'kegiatan/kegiatan_form',
                'button' => 'Ubah',
                'action' => site_url('kegiatan/update_action'),
		'id_kegiatan' => set_value('id_kegiatan', $row->id_kegiatan),
		'kode_rekening' => set_value('kode_rekening', $row->kode_rekening),
		'kegiatan' => set_value('kegiatan', $row->kegiatan),
		'id_bidang' => set_value('id_bidang', $row->id_bidang),
		'tahun' => set_value('tahun', $row->tahun),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kegiatan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kegiatan', TRUE));
        } else {
            $data = array(
		'kode_rekening' => $this->input->post('kode_rekening',TRUE),
		'kegiatan' => $this->input->post('kegiatan',TRUE),
		'id_bidang' => $this->input->post('id_bidang',TRUE),
		'tahun' => $this->input->post('tahun',TRUE),
	    );

            $this->Kegiatan_model->update($this->input->post('id_kegiatan', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('kegiatan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Kegiatan_model->get_by_id($id);

        if ($row) {
            $this->Kegiatan_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('kegiatan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kegiatan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kode_rekening', 'kode rekening', 'trim|required');
	$this->form_validation->set_rules('kegiatan', 'kegiatan', 'trim|required');
	$this->form_validation->set_rules('id_bidang', 'id bidang', 'trim|required');
	$this->form_validation->set_rules('tahun', 'tahun', 'trim|required');

	$this->form_validation->set_rules('id_kegiatan', 'id_kegiatan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Kegiatan.php */
/* Location: ./application/controllers/Kegiatan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-10-14 15:05:45 */
/* https://jualkoding.com */