<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Tte extends CI_Controller {

	public function index()
	{
		$this->rbac->check_operation_access();
		$data = array(
			'konten' => 'tte/view',
			'judul_page' => 'History Dokumen',
		);
		$this->load->view('v_index',$data);
	}

}