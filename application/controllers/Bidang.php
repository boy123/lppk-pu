<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bidang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Bidang_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'bidang/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bidang/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bidang/index.html';
            $config['first_url'] = base_url() . 'bidang/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bidang_model->total_rows($q);
        $bidang = $this->Bidang_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'bidang_data' => $bidang,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Bidang',
            'konten' => 'bidang/bidang_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Bidang_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_bidang' => $row->id_bidang,
		'bidang' => $row->bidang,
	    );
            $this->load->view('bidang/bidang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bidang'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Form Bidang',
            'konten' => 'bidang/bidang_form',
            'button' => 'Simpan',
            'action' => site_url('bidang/create_action'),
	    'id_bidang' => set_value('id_bidang'),
	    'bidang' => set_value('bidang'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'bidang' => $this->input->post('bidang',TRUE),
	    );

            $this->Bidang_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('bidang'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Bidang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Form Bidang',
                'konten' => 'bidang/bidang_form',
                'button' => 'Ubah',
                'action' => site_url('bidang/update_action'),
		'id_bidang' => set_value('id_bidang', $row->id_bidang),
		'bidang' => set_value('bidang', $row->bidang),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bidang'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_bidang', TRUE));
        } else {
            $data = array(
		'bidang' => $this->input->post('bidang',TRUE),
	    );

            $this->Bidang_model->update($this->input->post('id_bidang', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('bidang'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Bidang_model->get_by_id($id);

        if ($row) {
            $this->Bidang_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('bidang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bidang'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('bidang', 'bidang', 'trim|required');

	$this->form_validation->set_rules('id_bidang', 'id_bidang', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Bidang.php */
/* Location: ./application/controllers/Bidang.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-10-14 14:30:06 */
/* https://jualkoding.com */