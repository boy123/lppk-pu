
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
            </div>
        <div class="widget-body">
        <div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nip <?php echo form_error('nip') ?></label>
            <input type="text" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?php echo $nip; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Jabatan <?php echo form_error('id_jabatan') ?></label>
            <select name="id_jabatan" class="form-control" id="id_jabatan">
                <option value="">--Pilih Jabatan--</option>
                <?php 
                foreach ($this->db->get('jabatan')->result() as $rw): 
                    $checked = ($id_jabatan == $rw->id_jabatan) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_jabatan ?>" <?php echo $checked ?>><?php echo $rw->jabatan ?></option>
                <?php endforeach ?>
            </select>
	    <div class="form-group">
            <label for="int">Bidang <?php echo form_error('id_bidang') ?></label>
            <select name="id_bidang" class="form-control" id="id_bidang">
                <option value="">--Pilih Bidang--</option>
                <?php 
                foreach ($this->db->get('bidang')->result() as $rw): 
                    $checked = ($id_bidang == $rw->id_bidang) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_bidang ?>" <?php echo $checked ?>><?php echo $rw->bidang ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <input type="hidden" name="id_pegawai" value="<?php echo $id_pegawai; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pegawai') ?>" class="btn btn-default">Cancel</a>
	</form>
                                    </div>
                                </div>
                                </div>
<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#id_jabatan").select2();
        $("#id_bidang").select2();
    });
</script>