<?php 
$header = $data_header->row();
$id_bidang = $this->input->get('id_bidang');
$bulan = $this->input->get('bulan');
$tahun = $this->input->get('tahun');

 ?>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-lightred">
                <span class="widget-caption"><?php echo $judul_page ?></span>
            </div>
            <div class="widget-body">
                <div id="horizontal-form">
                    <form class="form-horizontal" action="rekapbidang/save_rekap_detail/<?php echo $this->uri->segment(3) ?>/?<?php echo param_get() ?>" method="POST" enctype="multipart/form-data" role="form">

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Uraian DPA * </label>
                            <div class="col-sm-10">
                                <select name="id_dpa" id="id_dpa" style="width:100%;" required>
                                    <option value="">--Pilih DPA --</option>
                                    <?php 
                                    $this->db->where('id_subkegiatan', $header->id_subkegiatan);
                                    foreach ($this->db->get('dpa')->result() as $row): ?>
                                        <option value="<?php echo $row->id_dpa ?>"><?php echo '['.$row->kode_rekening.'] '.$row->uraian ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Volume (optional)</label>
                            <div class="col-sm-2">
                                <input type="text"  class="form-control" id="volume" name="volume" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Lokasi (optional)</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="lokasi" name="lokasi" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Type (optional)</label>
                            <div class="col-sm-3">
                                <select name="type" id="type" style="width:100%;">
                                    <option value="">--Pilih Type--</option>
                                    <option value="Penunjukan Langsung">Penunjukan Langsung</option>
                                    <option value="Pengadaan Langsung">Pengadaan Langsung</option>
                                    <option value="Tender">Tender</option>
                                    <option value="Tender Cepat">Tender Cepat</option>
                                    <option value="E-Purchasing">E-Purchasing</option>
                                    <option value="E-Marketplace">E-Marketplace</option>
                                    <option value="Swakelola">Swakelola</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Penyedia Jasa (optional) </label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="penyedia_jasa" name="penyedia_jasa" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Nilai Kontrak (optional)</label>
                            <div class="col-sm-3">
                                <input type="text"  class="form-control" id="nilai_kontrak" name="nilai_kontrak" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Mulai Kontrak (optional)</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input class="form-control date-picker" id="mulai_kontrak" type="text" name="mulai_kontrak" data-date-format="yyyy-mm-dd" autocomplete="off">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Selesai Kontrak (optional)</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input class="form-control date-picker" id="selesai_kontrak" type="text" name="selesai_kontrak" data-date-format="yyyy-mm-dd" autocomplete="off">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Target (%) *</label>
                            <div class="col-sm-2">
                                <input type="number" step="0.01"  class="form-control" id="target" name="target" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Realiasasi (%) *</label>
                            <div class="col-sm-2">
                                <input type="number" step="0.01" class="form-control" id="realisasi" name="realisasi" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Nilai Penyerapan *</label>
                            <div class="col-sm-3">
                                <input type="text"  class="form-control" id="nilai_penyerapan" name="nilai_penyerapan" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Keterangan (optional)</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="ket" name="ket" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="foto" class="col-sm-2 control-label no-padding-right">Foto Kegiatan (optional)</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="foto">
                                <p style="color: red">*)max ukuran file 5MB</p>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="rekapbidang/detail/<?php echo $header->id_rekap_header ?>?<?php echo param_get() ?>" class="btn btn-default">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    
$(document).ready(function() {


    $("#id_dpa").select2();
    $("#type").select2();



});

var rupiah = document.getElementById("nilai_penyerapan");
var rupiah1 = document.getElementById("nilai_kontrak");
rupiah.addEventListener("keyup", function(e) {
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  rupiah.value = formatRupiah1(this.value);
});
rupiah1.addEventListener("keyup", function(e) {
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  rupiah1.value = formatRupiah(this.value);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }

  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
  return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}


/* Fungsi formatRupiah */
function formatRupiah1(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah1 = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    rupiah1 += separator + ribuan.join(".");
  }

  rupiah1 = split[1] != undefined ? rupiah1 + "," + split[1] : rupiah1;
  return prefix == undefined ? rupiah1 : rupiah1 ? "Rp. " + rupiah1 : "";
}

</script>
</script>