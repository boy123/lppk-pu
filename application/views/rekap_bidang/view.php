<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="" method="get" role="form">
                    <div class="form-group">
                        <select name="id_bidang" id="id_bidang" style="width:100%;" required="">
			                <option value="">--Pilih Bidang --</option>
			                <?php 
			                if ($this->session->userdata('level') == '7') {
			                	$id_pegawai = $this->session->userdata('keterangan');
			                	$id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
			                	$this->db->where('id_bidang', $id_bidang_user);
			                }
			                foreach ($this->db->get('bidang')->result() as $rw): 
			                    ?>
			                    <option value="<?php echo $rw->id_bidang ?>"><?php echo $rw->bidang ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>

                    <div class="form-group">
                    	<select name="bulan" id="bulan" style="width:100%;" required>
			                <option value="">--Pilih Bulan --</option>
			                <?php 
			                for ($i=1; $i <= 12; $i++) { 
			                	?>
			                	<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
			                	<?php
			                }
			                 ?>
			            </select>
                    </div>


                    <div class="form-group">
                    	<select name="tahun" id="tahun" style="width:100%;" required>
			                <option value="">--Pilih Tahun --</option>
			                <?php 
			                $this->db->order_by('tahun', 'desc');
			                // $this->db->where('tahun <=', date('Y'));
			                foreach ($this->db->get('tahun')->result() as $rw): 
			                    ?>
			                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>

                    
                    <button type="submit" class="btn btn-primary">FILTER</button>

                </form>
            </div>
        </div>
    </div>
</div>

<?php if (isset($_GET['id_bidang'])): 

$id_bidang = $this->input->get('id_bidang');
$bulan = $this->input->get('bulan');
$tahun = $this->input->get('tahun');

	?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Data Rekap Bidang</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<table class="table table-hover table-striped table-bordered">
        			<tr>
        				<td>Bidang</td>
        				<td><b><?php echo get_data('bidang','id_bidang',$id_bidang,'bidang') ?></b></td>
        			</tr>
        			<tr>
        				<td>Bulan</td>
        				<td><b><?php echo bulan_indo($bulan) ?></b></td>
        			</tr>
        			<tr>
        				<td>Tahun</td>
        				<td><b><?php echo $tahun ?></b></td>
        			</tr>
        			<tr>
        				<td>Clone Data</td>
        				<td>
        					<a href="rekapbidang/clone_dari_bulan_lalu?<?php echo param_get() ?>" class="btn btn-success" id="cloneBtn"><i class="fa fa-copy"></i> Clone data dari bulan lalu</a>
        				</td>
        			</tr>

        		</table>
        		<br>
        		<a href="cetak/cetak_rekap_bidang_header?<?php echo param_get() ?>&jenis_cetak=pdf" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Rekap Bidang PDF</a>
        		<a href="cetak/cetak_rekap_bidang_header?<?php echo param_get() ?>&jenis_cetak=excel" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Rekap Bidang Excel</a>
        			<a href="rekapkegiatan" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Rekap Perkegiatan</a>

        		<br>
        		<br>

                <a href="rekapbidang/create?<?php echo param_get() ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Rekap</a>



                <!-- <button class="btn btn-darkorange" id="btnImport">Import Data</button> -->
                <br>
                <br>
                
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

                <br>




                <div class="table-scrollable">
	                <table class="table table-bordered table-hover table-striped" id="searchable">
	                    <thead class="bordered-darkorange">
	                        <tr role="row">
	                            <th>PPTK</th>
	                            <th>Bidang</th>
	                            <th>Uraian</th>
	                            <th>Periode</th>
	                            <th>User at</th>
	                            <th>Date at</th>
	                            <th width="200px">Pilihan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php 
	                    $this->db->where('id_bidang', $id_bidang);
	                    $this->db->where('bulan', $bulan);
	                    $this->db->where('tahun', $tahun);
	                    $header = $this->db->get('rekap_header');
	                    foreach ($header->result() as $rw): ?>
	            
	                        <tr>
	                            <td><?php echo $rw->pptk ?></td>
	                            <td><?php echo get_data('bidang','id_bidang',$rw->id_bidang,'bidang') ?></td>
	                            <td><?php echo get_data('subkegiatan','id_subkegiatan',$rw->id_subkegiatan,'subkegiatan') ?></td>
	                            <td><?php echo bulan_indo($rw->bulan).'-'.$rw->tahun ?></td>
	                            <td><?php echo get_data('users','id_user',$rw->created_user,'nama') ?></td>
	                            <td><?php echo $rw->created_at ?></td>
	                            <td>
	                            	<a href="rekapbidang/detail/<?php echo $rw->id_rekap_header.'?'.param_get() ?>" class="label label-primary">Input Detail</a>
	                            	|
	                            	<a href="rekapbidang/update/<?php echo $rw->id_rekap_header.'?'.param_get() ?>" class="label label-info">Ubah</a>
	                            	|
	                            	<a onclick="javasciprt: return confirm('Apakah kamu yakin akan hapus data ini, semua data yang berhubungan dengan rekap ini akan di hapus ?')" href="rekapbidang/delete/<?php echo $rw->id_rekap_header.'?'.param_get() ?>" class="label label-danger">Hapus</a>
	                            </td>
	                            
	                            
	                        </tr>

	                    <?php endforeach ?>
	                        
	                    </tbody>
	                </table>
            	</div>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		<?php 
		if ($bulan == '1' OR $header->num_rows() > 0) {
		 ?>
			$("#cloneBtn").attr("disabled", true);
			$("#cloneBtn").attr("href", "rekapbidang?<?php echo param_get() ?>");
			$("#cloneBtn").attr("title", "Tidak bisa clone data dari bulan lalu");
		<?php } ?>
	});
</script>

<?php endif ?>


