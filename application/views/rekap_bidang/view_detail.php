<?php 
$header = $data_header->row();
$id_bidang = $this->input->get('id_bidang');
$bulan = $this->input->get('bulan');
$tahun = $this->input->get('tahun');

 ?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Input Detail Rekap Bidang</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<table class="table table-hover table-striped table-bordered">
        			<tr>
        				<td>PPTK</td>
        				<td><b><?php echo $header->pptk ?></b></td>
        			</tr>
        			<tr>
        				<td>Uraian</td>
        				<td><b><?php echo get_data('subkegiatan','id_subkegiatan',$header->id_subkegiatan,'subkegiatan') ?></b></td>
        			</tr>
        			<tr>
        				<td>Bidang</td>
        				<td><b><?php echo get_data('bidang','id_bidang',$id_bidang,'bidang') ?></b></td>
        			</tr>
        			<tr>
        				<td>Bulan</td>
        				<td><b><?php echo bulan_indo($bulan) ?></b></td>
        			</tr>
        			<tr>
        				<td>Tahun</td>
        				<td><b><?php echo $tahun ?></b></td>
        			</tr>

        		</table>

        		<br>

                <a href="rekapbidang/input_detail/<?php echo $header->id_rekap_header ?>?<?php echo param_get() ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                <a href="rekapbidang?<?php echo param_get() ?>" class="btn btn-warning"><i class="fa fa-forward"></i> Kembali</a>



                <!-- <button class="btn btn-darkorange" id="btnImport">Import Data</button> -->
                <br>
                <br>
                
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

                <br>




                <div class="table-scrollable">
	                <table class="table table-bordered table-hover table-striped" id="searchable">
	                    <thead class="bordered-darkorange">
	                        <tr role="row">
	                            <th>No</th>
	                            <th>Uraian</th>
	                            <th>Dana DPA</th>
	                            <th>DPA-P</th>
	                            <th>Lokasi</th>
	                            <th>Penyedia Jasa</th>
	                            <th>Nilai Kontrak</th>
	                            <th>Mulai Kontrak</th>
	                            <th>Selesai Kontrak</th>
	                            <th>Target</th>
	                            <th>Realisasi</th>
	                            <th>Nilai Penyerapan</th>
	                            <th>Persentase</th>
	                            <th>Ket</th>
	                            <th>Foto</th>
	                            <th width="200px">Pilihan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php 
	                    $no = 1;
	                    $this->db->order_by('created_at', 'desc');
	                    $this->db->where('id_rekap_header', $header->id_rekap_header);
	                    foreach ($this->db->get('rekap_detail')->result() as $rw): ?>
	            
	                        <tr>
	                        	<td><?php echo $no ?></td>
	                            <td><?php echo get_data('dpa','id_dpa',$rw->id_dpa,'uraian') ?></td>
	                            <td><?php echo angka_indo(get_data('dpa','id_dpa',$rw->id_dpa,'jumlah')) ?></td>
	                            <td><?php echo angka_indo(get_data('dpa','id_dpa',$rw->id_dpa,'dpap')) ?></td>
	                            <td><?php echo $rw->lokasi ?></td>
	                            <td><?php echo $rw->penyedia_jasa ?></td>
	                            <td><?php echo angka_indo($rw->nilai_kontrak) ?></td>
	                            <td><?php echo $rw->mulai_kontrak ?></td>
	                            <td><?php echo $rw->selesai_kontrak ?></td>
	                            <td><?php echo $rw->target ?></td>
	                            <td><?php echo $rw->realisasi ?></td>
	                            <td><?php echo angka_indo($rw->nilai_penyerapan) ?></td>
	                            <td><?php echo $rw->persen_penyerapan ?></td>
	                            <td><?php echo $rw->ket ?></td>
	                            <td>
	                            	<?php if ($rw->foto != ''): ?>
	                            		<a href="image/kegiatan/<?php echo $rw->foto ?>" target="_blank" class="label label-success">Lihat Foto</a>
	                            	<?php endif ?>
	                            </td>
	                            <td>
	                            	<?php if ($this->session->userdata('level') == 1): ?>
	                            		<a onclick="javasciprt: return confirm('Apakah kamu yakin akan perbaiki data ini ?')" href="rekapbidang/perbaiki_parent_dpa_direkap/<?php echo $rw->id_rekap_header.'/'.$rw->id_dpa.'?'.param_get() ?>" class="label label-warning">Perbaiki Parent Uraian</a>
	                            	<?php endif ?>

	                            	<a href="rekapbidang/edit_detail/<?php echo $rw->id_rekap_detail.'?'.param_get() ?>" class="label label-info">Ubah</a>
	                            	|
	                            	<a onclick="javasciprt: return confirm('Apakah kamu yakin akan hapus data ini, semua data yang berhubungan dengan rekap ini akan di hapus ?')" href="rekapbidang/delete_detail/<?php echo $rw->id_rekap_detail.'/'.$rw->id_rekap_header.'?'.param_get() ?>" class="label label-danger">Hapus</a>
	                            </td>
	                            
	                            
	                        </tr>

	                    <?php $no++; endforeach ?>
	                        
	                    </tbody>
	                </table>
            	</div>


            </div>
        </div>
    </div>
</div>