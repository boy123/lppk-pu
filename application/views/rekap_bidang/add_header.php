<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-lightred">
                <span class="widget-caption"><?php echo $judul_page ?></span>
            </div>
            <div class="widget-body">
                <div id="horizontal-form">
                    <form class="form-horizontal" action="rekapbidang/create_action?<?php echo param_get() ?>" method="POST" role="form">

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">PPTK * </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pptk" value="<?php echo $this->session->userdata('nama'); ?>" name="pptk" readonly required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Uraian *</label>
                            <div class="col-sm-10">
                                <select name="id_subkegiatan" id="sub_kegiatan" style="width:100%;" required>
                                <option value="">--Pilih Sub Kegiatan --</option>
                                <?php 
                                $id_bidang = $this->input->get('id_bidang');
                                $tahun = $this->input->get('tahun');
                                $sql = "
                                SELECT a.*
                                FROM subkegiatan as a 
                                INNER JOIN kegiatan as b ON b.id_kegiatan = a.id_kegiatan
                                where b.id_bidang='$id_bidang' and b.tahun = '$tahun'
                                ";
                                foreach ($this->db->query($sql)->result() as $row): ?>
                                    <option value="<?php echo $row->id_subkegiatan ?>"><?php echo '['.$row->kode_rekening.'] '.$row->subkegiatan ?></option>
                                <?php endforeach ?>
                            </select>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="rekapbidang?<?php echo param_get() ?>" class="btn btn-default">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    
$(document).ready(function() {


    $("#sub_kegiatan").select2();



});

</script>