<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
 
  <!--Meta-->
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="eMonev Dinas PUPR Provinsi Jambi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!--Favicon-->
  <link rel="icon" href="front/pu.jpg">
  <base href="<?php echo base_url() ?>">
  
  <!-- Title-->
  <title>Emonev PUPR Provinsi Jambi </title>
  
  <!--Google fonts-->
  <link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700%7COpen+Sans:400,600,700" rel="stylesheet">
  
	<!--icon fonts-->
	<link rel="stylesheet" href="front/assets/vendor/strokegap/style.css">
	<link rel="stylesheet" href="front/monev2017/font/css/font-awesome.min.html">
	<link rel="stylesheet" href="front/assets/vendor/linearicons/style.css">
  
  <!-- Stylesheet-->
  <link rel="stylesheet" href="front/assets/vendor/bootstrap/dist/css/bootstrap.min.css">  
  <link rel="stylesheet" href="front/assets/vendor/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="front/assets/vendor/fancybox/dist/jquery.fancybox.min.css">
  <link rel="stylesheet" href="front/assets/vendor/animate.css/animate.min.css">
  
  <link rel="stylesheet" href="front/assets/css/style.css">

	  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P3KQL2H');</script>
	<!-- End Google Tag Manager -->
  
</head>

<script async src="https://www.googletagmanager.com/gtag/js?id=G-30XMX1J95B"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-30XMX1J95B');
</script>
       
 <body id="top">
 <!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->
 
<!--[if lt IE 8]>
<p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->



 <header class="header header-shrink header-inverse fixed-top">
  <div class="container">
		<nav class="navbar navbar-expand-lg px-md-0">
			<a class="navbar-brand" href="web">
				<span class="logo-default">
					<img src="front/logo.png" alt="">
				</span>
				<span class="logo-inverse">
					<img src="front/logo.png" alt="">
				</span>
			</a>

			<button class="navbar-toggler p-0" data-toggle="collapse" data-target="#navbarNav">
              <div class="hamburger hamburger--spin js-hamburger">
                    <div class="hamburger-box">
                      <div class="hamburger-inner"></div>
                    </div>
                </div>
			</button>

			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#" data-scrollto="top">BERANDA</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#" data-scrollto="about">TENTANG</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#" data-scrollto="features">TAHUN</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#" data-scrollto="faq">e-PLANNING</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#" data-scrollto="contact">KONTAK</a>
					</li>
					<!-- <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Tautan <i class="lnr lnr-chevron-down u-fs-12 ml-1"></i>
						</a>
						<div class="dropdown-menu box-shadow-v2" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" target="_blank" href="https://edata.surabaya.go.id/">E-Data</a>
							<a class="dropdown-item" target="_blank" href="https://sigis.surabaya.go.id/">E-Sigis</a>
						</div>
					</li> -->
					<li class="nav-item">
						<a class="nav-link btn btn btn-rounded btn-primary u-w-170 ml-lg-4 text-capitalize" target="_blank" href="login">Login</a>
					</li>
				</ul>
			</div>

		</nav>
  </div> <!-- END container-->
</header> <!-- END header -->






<section class="u-py-100 u-h-100vh u-flex-center" style="background:url(front/kantor-min.jpg) no-repeat; background-size: cover; background-position: center center;">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-8 mt-5">
        <h1 class="display-3 u-fw-600 text-primary">
          Welcome to <span class="text-yellow" data-type="MONEV PUPR, PROVINSI JAMBI, MONITORING, EVALUASI, PERENCANAAN, PUPR, PROVINSI, JAMBI"></span>
        </h1>
        <p class="u-fs-22 text-yellow u-lh-1_7 u-py-40">
         Sistem informasi monitoring dan evaluasi merupakan sistem berbasis web digunakan untuk monitoring dan evaluasi realisasi dan capaian indikator (Tujuan,Sasaran,Program dan kegiatan) perencanaan Provinsi Jambi
        </p>
        <!-- <a href="#" class="btn btn btn-rounded btn-yellow u-w-160">
        	Download Now
        </a>
        <a href="#" class="btn btn btn-rounded btn-white ml-3 u-w-160">
        	App Features
        </a> -->
      </div>
      
      <div class="col-lg-4 ml-auto text-center mt-5">
        <!-- <img src="front/content-07.png" alt="" height="100%" width="100%"> -->
      </div>
      
    </div> <!-- END row-->
  </div> <!-- END container-->
  
  <div class="u-pos-abs u-bottom-0 u-z-index-minus-1 u-left-0">
    <img src="front/assets/img/app/curve.png" alt="">
  </div>
</section> <!-- END intro-hero-->






<section id="about">
    <div class="container">
      <div class="row align-items-center">
       
        <div class="col-lg-5 text-center">
          <img class="wow fadeInLeft" src="front/content-04.png" alt="">
        </div> <!-- END col-lg-5 -->
        
        <div class="col-lg-6 ml-auto">
          <h2 class="h1">
            EMONEV TSPK (Tujuan,Sasaran,Program dan Kegiatan)
          </h2>
          <div class="u-h-4 u-w-50 bg-primary rounded mt-4 mb-5"></div>
          <p class="mb-5">
            Sistem informasi monitoring dan evaluasi merupakan sistem berbasis web digunakan untuk monitoring dan evaluasi realisasi dan capaian indikator (Tujuan,Sasaran,Program dan kegiatan) perencanaan Provinsi Jambi
          </p>
        </div> <!-- END col-lg-6 ml-auto-->
        
      </div> <!-- END row-->
    </div> <!-- END container-->
</section> <!-- END section-->



<hr>

<section>
	<div class="container">
		<div class="row align-items-center">

			<div class="col-lg-6">
				<h2 class="h1">
					Fungsi <span class="text-primary">MONEV TSPK</span>
				</h2>
				<div class="u-h-4 u-w-50 bg-primary rounded mt-4 mb-5"></div>
				<p class="mb-5">
					Sistem informasi monitoring dan evaluasi merupakan sistem berbasis web digunakan untuk monitoring dan evaluasi realisasi dan capaian indikator (Tujuan,Sasaran,Program dan kegiatan) perencanaan Provinsi Jambi, mempunyai fungsi sebagai berikut
				</p>
				<ul class="list-unstyled u-fw-600 u-lh-2">
					<li>
						<i class="fa fa-check mr-2 color-primary"></i>
						Monitoring realisasi dan capaian indikator perencanaan kota
					</li>
					<li>
						<i class="fa fa-check mr-2 color-primary"></i>
						Evaluasi terkait perencanaan kota 
					</li>
					<li>
						<i class="fa fa-check mr-2 color-primary"></i>
						Perhitungan nilai realisasi indikator (Tujuan,Sasaran,Program dan Kegiatan)
					</li>
				</ul>
			</div> <!-- END col-lg-6 ml-auto-->

			<div class="col-lg-6 pl-lg-5 ml-auto text-center">
				<img class="wow fadeInRight" src="front/content-01.png" alt="">
			</div> <!-- END col-lg-4 -->

		</div> <!-- END row-->
	</div> <!-- END container-->
</section> <!-- END section-->

     
          
                 
                    
                              
<section id="features" class="bg-gray-v1">
  <div class="container">
    <div class="row text-center">
    
     <div class="col-12">
       <h2 class="h1">
       	TAHUN APLIKASI
       </h2>
       <div class="u-h-4 u-w-50 bg-primary rounded mt-4 u-mb-40 mx-auto"></div>
     </div>
     
      <div class="col-lg-4 col-md-6 u-mt-30">
        <a href="2019.html" target="_blank">
        <div class="bg-white px-4 py-5 px-md-5 u-h-100p rounded box-shadow-v1">
          <div class="u-w-90 u-h-90 u-flex-center bg-red text-white rounded-circle m-auto">
            <span class="fa fa-folder-open u-fs-42"></span>
          </div>
          <h4 class="u-fs-26 u-pt-30 u-pb-20">
            Tahun 2019  
          </h4>
          <p>
            Sistem informasi monitoring evaluasi tahun perencanaan 2019
          </p>
        </div>
      </a>
      </div> <!-- END col-lg-4 col-md-6-->
      
      <div class="col-lg-4 col-md-6 u-mt-30">
        <a href="monev2018.html" target="_blank">
          <div class="bg-white px-4 py-5 px-md-5 u-h-100p rounded box-shadow-v1">
            <div class="u-w-90 u-h-90 u-flex-center bg-primary text-white rounded-circle m-auto">
              <span class="fa fa-folder-open u-fs-42"></span>
            </div>
            <h4 class="u-fs-26 u-pt-30 u-pb-20">
              Tahun 2018
            </h4>
            <p>
              
              Sistem informasi monitoring evaluasi tahun perencanaan 2018
            </p>
          </div>
        </a>
      </div> <!-- END col-lg-4 col-md-6-->
      
      <div class="col-lg-4 col-md-6 u-mt-30">
        <a href="monev2017.html" target="_blank">
          <div class="bg-white px-4 py-5 px-md-5 u-h-100p rounded box-shadow-v1">
            <div class="u-w-90 u-h-90 u-flex-center bg-yellow text-white rounded-circle m-auto">
              <span class="fa fa-folder-open u-fs-42"></span>
            </div>
            <h4 class="u-fs-26 u-pt-30 u-pb-20">
              Tahun 2017
            </h4>
            <p>
              
              Sistem informasi monitoring evaluasi tahun perencanaan 2017
            </p>
          </div>
        </a>
      </div> <!-- END col-lg-4 col-md-6-->
      
    
      
    </div> <!-- END row-->
  </div> <!-- END container-->
</section> <!-- END section-->

    
        
            
  
          
               
                    
                              
<section data-init="parallax" style="background:url(front/kantor-min.jpg) no-repeat fixed; background-size:cover; background-position: center center;">
  <div class="overlay bg-blue-opacity"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6 m-auto text-center">
        <h2 class="h1 text-white u-mb-40">
          <center><img src="front/pu.jpg" width="30%"></center>
          Dinas PUPR Provinsi Jambi
        </h2>
      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section> <!-- END section-->

    
        
            
						               
  
   
     
               
            
   
<!-- <section id="faq" class="bg-gray-v1">  
	 <div class="container">
	      <div class="row align-items-center">
	       
	        <div class="col-lg-5 text-center">
	          <img class="wow fadeInLeft" src="front/eplanning.png" alt="">
	        </div> 
	        
	        <div class="col-lg-6 ml-auto">
	          <h2 class="h1">
	            E-PLANNING Provinsi Jambi
	          </h2>
	          <div class="u-h-4 u-w-50 bg-primary rounded mt-4 mb-5"></div>
	          <p class="mb-5">
	            Aplikasi E-Planning adalah sistem aplikasi pendukung penyusunan dokumen perencanaan jangka menegah dan jangka pendek yang telah dikembangkan oleh Pemerintah Provinsi Jambi dalam rangka mewujudkan perencanaan pembangunan yang selaras dan akuntabel.
	          </p>
	          <a href="https://bappeko.surabaya.go.id/eplanning" target="_blank" class="btn btn btn-rounded btn-red px-4">
	            <i class="fa fa-link mr-1"></i>
	             Lihat aplikasi e-planning
	          </a>
	        </div> 
	        
	      </div> 
	    </div> 
</section>  -->
      
      
      
      
      
      
    
 
  
  
  
 
 
 
<footer  id="contact" style="background:url(front/assets/img/app/footer.jpg) no-repeat; background-size: cover; background-position: center center;">
	<div class="container">
	
		<div class="row">
		
			<!-- <div class="col-12">
				<div class="bg-white box-shadow-v1 rounded p-5">
					<h2 class="h1 text-center">
						HUBUNGI KAMI
					</h2>
					<br>
					<br>

					<div class="container">
							<div class="row">
								<div class="col-md-4 mb-4">
									<div class="media align-items-center justify-content-center">
										<span class="icon icon-Phone2 u-fs-lg-60 u-fs-40 0 text-primary mr-3"></span>
										<div class="media-body">
											<p class="u-fs-lg-24 color-heading mb-1">(031)5312144</p>
											<p class="mb-0">Bidang evalitbang </p>
										</div>
									</div> 
								</div> 
								
								<div class="col-md-4 mb-4">
									<div class="media align-items-center">
										<span class="icon icon-Mail u-fs-6-lg0 t u-fs-40 text-primary mr-3"></span>
										<div class="media-body">
											<p class="u-fs-lg-24 color-heading mb-1">bappeko@surabaya.go.id</p>
											<p class="mb-0">email support</p>
										</div>
									</div>
								</div> 
								
								<div class="col-md-4 mb-4">
									<div class="media align-items-center">
										<span class="icon icon-Pointer u-f-lgs-6 u-fs-40 text-primary mr-3"></span>
										<div class="media-body">
											<p class="u-fs-lg-24 color-heading mb-1">Jl. Pacar No 8</p>
											<p class="mb-0">Provinsi Jambi</p>
										</div>
									</div> 
								</div> 
								
								<div class="col-md-4"></div>
								<div class="col-md-4"></div>
							</div>
	</div> 

				</div>
			</div> -->
			
			<div class="col-12 text-center u-py-60"> 
				<img src="front/logo.png" alt="">
				<ul class="list-inline social social-rounded mt-4">
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-facebook"></i></a>
					</li>
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-twitter"></i></a>
					</li>
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-google-plus"></i></a>
					</li>
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-linkedin"></i></a>
					</li>
					<li class="list-inline-item">
						<a href="#"><i class="fa fa-dribbble"></i></a>
					</li>
				</ul>
			</div> <!-- END col-12 -->
			
		</div> <!-- END row-->
		
		
		<div class="row align-items-center" style="border-top:1px solid #339aff;color:#a9d5ff;">
			<div class="col-lg-6 py-4 text-center text-lg-left">
				<span>© Copyright <?php echo date('Y') ?>  -  Dinas PUPR Provinsi Jambi</span>
			</div>
		
		</div> <!-- END row-->
	</div> <!-- END container-->
</footer> <!-- END footer-->





		<div class="scroll-top bg-white box-shadow-v1">
			<i class="fa fa-angle-up" aria-hidden="true"></i>
		</div>

	
		<script src="front/assets/js/bundle.js"></script>
		<script src="front/assets/js/fury.js"></script>
  </body>	
</html>
