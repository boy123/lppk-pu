<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="" method="get" role="form">
                    <div class="form-group">
                        <select name="id_bidang" id="id_bidang" style="width:100%;" required="">
			                <option value="">--Pilih Bidang --</option>
			                <?php 
			                if ($this->session->userdata('level') == '7') {
				                $id_pegawai = $this->session->userdata('keterangan');
				                $id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
				                $this->db->where('id_bidang', $id_bidang_user);
				            }
			                foreach ($this->db->get('bidang')->result() as $rw): 
			                    ?>
			                    <option value="<?php echo $rw->id_bidang ?>"><?php echo $rw->bidang ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>

                    <div class="form-group">
                    	<select name="tahun" id="tahun" style="width:100%;" required>
			                <option value="">--Pilih Tahun --</option>
			                <?php 
			                $this->db->order_by('tahun', 'desc');
			                $this->db->group_by('tahun');
			                foreach ($this->db->get('kegiatan')->result() as $rw): 
			                    ?>
			                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>

                    <div class="form-group">
                    	<select name="kegiatan" id="kegiatan" style="width:100%;" required>
			                <option value="">--Pilih Kegiatan --</option>
			            </select>
                    </div>

                    <div class="form-group">
                    	<select name="sub_kegiatan" id="sub_kegiatan" style="width:100%;" required>
			                <option value="">--Pilih Sub Kegiatan --</option>
			            </select>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">FILTER</button>

                </form>
            </div>
        </div>
    </div>
</div>

<?php if (isset($_GET['id_bidang'])): 

$id_bidang = $this->input->get('id_bidang');
$tahun = $this->input->get('tahun');
$id_kegiatan = $this->input->get('kegiatan');
$id_subkegiatan = $this->input->get('sub_kegiatan');

	?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Data DPA</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<table class="table table-hover table-striped table-bordered">
        			<tr>
        				<td>Bidang</td>
        				<td><b><?php echo get_data('bidang','id_bidang',$id_bidang,'bidang') ?></b></td>
        			</tr>
        			<tr>
        				<td>Tahun</td>
        				<td><b><?php echo $tahun ?></b></td>
        			</tr>
        			<tr>
        				<td>Kegiatan</td>
        				<td><b><?php echo get_data('kegiatan','id_kegiatan',$id_kegiatan,'kegiatan') ?></b></td>
        			</tr>
        			<tr>
        				<td>Subkegiatan</td>
        				<td><b><?php echo get_data('subkegiatan','id_subkegiatan',$id_subkegiatan, 'subkegiatan') ?></b></td>
        			</tr>
        		</table>

        		<br>

                <a href="dpa/create?<?php echo param_get() ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah DPA</a>



                <!-- <button class="btn btn-darkorange" id="btnImport">Import Data</button> -->
                <br>
                <br>
                
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

                <br>




                <div class="table-scrollable">
	                <table class="table table-bordered table-hover table-striped" id="searchable">
	                    <thead class="bordered-darkorange">
	                        <tr role="row">
	                            <th>Kode Rekening</th>
	                            <th>Uraian</th>
	                            <th>Jumlah</th>
	                            <th>DPAP</th>
	                            <th>Sub Uraian dari</th>
	                            <th width="200px">Pilihan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php 
	                    $this->db->where('id_subkegiatan', $id_subkegiatan);
	                    foreach ($this->db->get('dpa')->result() as $rw): ?>
	            
	                        <tr>
	                            <td><?php echo $rw->kode_rekening ?></td>
	                            <td><?php echo $rw->uraian ?></td>
	                            <td><?php echo number_format($rw->jumlah) ?></td>
	                            <td><?php echo number_format($rw->dpap) ?></td>
	                            <td><?php echo get_data('dpa','id_dpa',$rw->parent,'uraian') ?></td>
	                            <td>
	                            	<a href="dpa/update/<?php echo $rw->id_dpa.'?'.param_get() ?>" class="label label-info">Ubah</a>
	                            	|
	                            	<a onclick="javasciprt: return confirm('Apakah kamu yakin akan hapus data dpa ini, semua data yang berhubungan dengan dpa ini akan di hapus ?')" href="dpa/delete/<?php echo $rw->id_dpa.'?'.param_get() ?>" class="label label-danger">Hapus</a>
	                            </td>
	                            
	                            
	                        </tr>

	                    <?php endforeach ?>
	                        
	                    </tbody>
	                </table>
            	</div>


            </div>
        </div>
    </div>
</div>

<?php endif ?>



<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
	
$(document).ready(function() {
    $("#kegiatan").select2();

    $("#tahun").change(function() {
        var bidang = $("#id_bidang").val();
        var tahun = $(this).val();
        $.ajax({url: "dpa/getListKegiatan/"+bidang+"/"+tahun, 
            beforeSend: function(){
                $(".loading-container").show();
                $(".loader").show();
            },
            success: function(result){
                $("#kegiatan").html(result);
              console.log("success");
            },
            complete:function(data){
                $(".loading-container").hide();
                $(".loader").hide();
            }
        });
        
    });


    $("#sub_kegiatan").select2();

    $("#kegiatan").change(function() {
        var id_kegiatan = $(this).val();
        $.ajax({url: "dpa/getListSubkegiatan/"+id_kegiatan, 
            beforeSend: function(){
                $(".loading-container").show();
                $(".loader").show();
            },
            success: function(result){
                $("#sub_kegiatan").html(result);
              console.log("success");
            },
            complete:function(data){
                $(".loading-container").hide();
                $(".loader").hide();
            }
        });
        
    });



});

</script>