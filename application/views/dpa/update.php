<?php 
$dpa = $data->row();
 ?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-lightred">
                <span class="widget-caption"><?php echo $judul_page ?></span>
            </div>
            <div class="widget-body">
                <div id="horizontal-form">
                    <form class="form-horizontal" action="dpa/update_action/<?php echo $dpa->id_dpa ?>?<?php echo param_get() ?>" method="POST" role="form">

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Kode Rekening</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="kode_rekening" name="kode_rekening" value="<?php echo $dpa->kode_rekening ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Uraian *</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="uraian" id="uraian"><?php echo $dpa->uraian ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Jumlah *</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="jumlah" value="<?php echo $dpa->jumlah ?>" name="jumlah" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">DPAP (optional)</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" id="dpap" value="<?php echo $dpa->dpap ?>" name="dpap" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right">Sub Uraian dari (optional)</label>
                            <div class="col-sm-10">
                                <select name="parent" id="parent" style="width:100%;">
                                    <option value="">--Pilih Sub Parent --</option>
                                    <?php 
                                    $this->db->where('parent IS NULL', null, false);
                                    $this->db->where('id_dpa !=', $dpa->id_dpa);
                                    $this->db->where('id_subkegiatan', $this->input->get('sub_kegiatan'));
                                    foreach ($this->db->get('dpa')->result() as $row): 
                                        $checked = ($dpa->parent == $row->id_dpa) ? 'selected' : '' ;
                                        ?>
                                        <option value="<?php echo $row->id_dpa ?>" <?php echo $checked ?>><?php echo '['.$row->kode_rekening.'] '.$row->uraian ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="dpa?<?php echo param_get() ?>" class="btn btn-default">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    
$(document).ready(function() {


    $("#parent").select2();



});
</script>

<script type="text/javascript">

var rupiah = document.getElementById("jumlah");
rupiah.addEventListener("keyup", function(e) {
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  rupiah.value = formatRupiah(this.value);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }

  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
  return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}

</script>