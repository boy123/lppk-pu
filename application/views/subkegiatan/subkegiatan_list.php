
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-yellow">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            <br>
            <div class="row">
            <div class="col-md-4">
                <?php echo anchor(site_url('subkegiatan/create'),'<i class="fa fa-plus"></i> Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                
            </div>
        </div>
        <br>
        <div class="table-scrollable">
        <table class="table table-bordered table-hover table-striped" style="margin-bottom: 10px" id="searchable">
            <thead class="bordered-darkorange">
                <tr role="row">
                    <th>No</th>
                    <th>Tahun</th>
		<th>Kegiatan</th>
		<th>Kode Rekening</th>
		<th>Subkegiatan</th>
		<th>Action</th>
                </tr>
            </thead>
            <tbody><?php
            $where = "";
            if ($this->session->userdata('level') == '7') {
                $id_pegawai = $this->session->userdata('keterangan');
                $id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
                $where .= "where b.id_bidang='$id_bidang_user'";
            }
            $sql = "
                SELECT a.*
                FROM subkegiatan as a 
                INNER JOIN kegiatan as b ON b.id_kegiatan = a.id_kegiatan
                $where
                ";
            foreach ($this->db->query($sql)->result() as $subkegiatan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
            <td><?php echo get_data('kegiatan','id_kegiatan',$subkegiatan->id_kegiatan,'tahun') ?></td>
			<td><?php echo get_data('kegiatan','id_kegiatan',$subkegiatan->id_kegiatan,'kegiatan') ?></td>
			<td><?php echo $subkegiatan->kode_rekening ?></td>
			<td><?php echo $subkegiatan->subkegiatan ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('subkegiatan/update/'.$subkegiatan->id_subkegiatan),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('subkegiatan/delete/'.$subkegiatan->id_subkegiatan),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        </div>
        <br>
       <!--  <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div> -->
    </div>
        </div>
    </div>
</div>
    