
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
            </div>
        <div class="widget-body">
        <div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Kegiatan <?php echo form_error('id_kegiatan') ?></label>
            <select name="id_kegiatan" class="form-control" id="id_kegiatan">
                <option value="">--Pilih Kegiatan--</option>
                <?php 
                if ($this->session->userdata('level') == '7') {
                    $id_pegawai = $this->session->userdata('keterangan');
                    $id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
                    $this->db->where('id_bidang', $id_bidang_user);
                }
                foreach ($this->db->get('kegiatan')->result() as $rw): 
                    $checked = ($id_kegiatan == $rw->id_kegiatan) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_kegiatan ?>" <?php echo $checked ?>><?php echo $rw->tahun." - ".$rw->kegiatan ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Kode Rekening <?php echo form_error('kode_rekening') ?></label>
            <input type="text" class="form-control" name="kode_rekening" id="kode_rekening" placeholder="Kode Rekening" value="<?php echo $kode_rekening; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Subkegiatan <?php echo form_error('subkegiatan') ?></label>
            <input type="text" class="form-control" name="subkegiatan" id="subkegiatan" placeholder="Subkegiatan" value="<?php echo $subkegiatan; ?>" />
        </div>
	    <input type="hidden" name="id_subkegiatan" value="<?php echo $id_subkegiatan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('subkegiatan') ?>" class="btn btn-default">Cancel</a>
	</form>
                                    </div>
                                </div>
                                </div>

<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#id_kegiatan").select2();
    });
</script>