<?php 
wajib_ubah_password();
 ?>
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success" style="background-color: #0c15bd">
			<h2>Selamat datang, <?php echo $this->session->userdata('nama'); ?></h2>
		</div>
		
	</div>

</div>

<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<h5 class="row-title"><i class="fa fa-tags blue"></i>Persentase Bulan ini</h5>
        <div class="row">
                
            <?php 
            $dataHome = getRekapDinas(date('m'), date('Y'));
            $target = 0;
            $realisasi = 0;
            $deviasi = 0;
            if (date('m') > 1) {
                $dataHome = getRekapDinas(date('m')-1, date('Y'));
                $target = $dataHome['target'];
                $realisasi = $dataHome['realisasi'];
                $deviasi = $dataHome['deviasi'];
            }
            // $dataHome = getRekapDinas(01, 2023);
             ?>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="databox bg-white radius-bordered">
                    <div class="databox-left bg-themethirdcolor">
                        <div class="databox-piechart">
                            <div data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="0" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.2)"><span class="white font-90"><?php echo $target; ?>%</span></div>
                        </div>
                    </div>
                    <div class="databox-right">
                        <span class="databox-number themethirdcolor">RENCANA</span>
                        <div class="databox-text darkgray"></div>
                        <div class="databox-stat themethirdcolor radius-bordered">
                            <i class="stat-icon  icon-lg fa fa-envelope-o"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="databox bg-white radius-bordered">
                    <div class="databox-left bg-themeprimary">
                        <div class="databox-piechart">
                            <div id="users-pie" data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="0" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.1)"><span class="white font-90"><?php echo $realisasi; ?>%</span></div>
                        </div>
                    </div>
                    <div class="databox-right">
                        <span class="databox-number themeprimary">REALISASI</span>
                        <div class="databox-text darkgray"></div>
                        <div class="databox-state bg-themeprimary">
                            <i class="fa fa-check"></i>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="databox bg-white radius-bordered">
                    <div class="databox-left bg-themesecondary">
                        <div class="databox-piechart">
                            <div data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="0" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.1)"><span class="white font-90"><?php echo $deviasi; ?>%</span></div>
                        </div>
                    </div>
                    <div class="databox-right">
                        <span class="databox-number themesecondary">DEVIASI</span>
                        <div class="databox-text darkgray"></div>
                        <div class="databox-stat themesecondary radius-bordered">
                            <i class="stat-icon icon-lg fa fa-tasks"></i>
                        </div>
                    </div>
                </div>
            </div>

            
            
            
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="rekapbidang" class="btn btn-labeled btn-block btn-info" style="height:50px"><i class="fa fa-print"></i>CETAK REKAP BIDANG</a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="rekapkegiatan" class="btn btn-labeled btn-block btn-danger" style="height:50px"><i class="fa fa-print"></i>CETAK REKAP PERKEGIATAN</a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="rekapdinas" class="btn btn-labeled btn-block btn-primary" style="height:50px"><i class="fa fa-print"></i>CETAK REKAP DINAS</a>
            </div>
        </div>
    </div>
</div>
<?php 
$tahun = date('Y');
 ?>
<div class="row" style="visibility: hidden;">
    <div class="col-md-8">
        <h2>Top Bidang dengan input LPPK paling duluan di <?php echo $tahun ?></h2>
        <canvas id="myChartLppk" width="400" height="200"></canvas>
    </div>
    <div class="col-md-4">
        <h2>Top Bidang dengan input e-Awasi paling banyak di <?php echo $tahun ?></h2>
        <canvas id="myChartAwasi" width="400" height="200"></canvas>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!--Easy Pie Charts Needed Scripts-->
<script src="assets/js/charts/easypiechart/jquery.easypiechart.js"></script>
<script src="assets/js/charts/easypiechart/easypiechart-init.js"></script>
<script>
        // Data hasil query (contoh)
        const bidangLppk = [
            <?php 
            foreach ($this->db->get('bidang')->result() as $rw) {
                echo "\"$rw->bidang\",";
            }
             ?>
            ];
        const totalInputLppk = [170, 30, 170, 90, 190, 30, 120, 130, 130];

        // Konfigurasi Chart.js
        const ctxLppk = document.getElementById('myChartLppk').getContext('2d');
        const myChartLppk = new Chart(ctxLppk, {
            type: 'bar', // Jenis grafik
            data: {
                labels: bidangLppk, // Label untuk bidang
                datasets: [{
                    label: 'Total Input',
                    data: totalInputLppk, // Data total input
                    backgroundColor: [
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(201, 203, 207, 0.2)',
                        'rgba(93, 173, 226, 0.2)'
                    ],
                    borderColor: [
                        'rgba(75, 192, 192, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(201, 203, 207, 1)',
                        'rgba(93, 173, 226, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    title: {
                        display: true,
                        text: 'Top Bidang <?php echo $tahun ?>'
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            stepSize: 10
                        }
                    }
                }
            }
        });

        const bidangAwasi = [
            'BINA MARGA',
            'PERUMAHAN',
            'SUMBER DAYA AIR',
            'CIPTA KARYA',
            ];
        const totalInputAwasi = [32.72, 14.78, 21.50, 0.0];

        // Konfigurasi Chart.js
        const ctxAwasi = document.getElementById('myChartAwasi').getContext('2d');
        const myChartAwasi = new Chart(ctxAwasi, {
            type: 'bar', // Jenis grafik
            data: {
                labels: bidangAwasi, // Label untuk bidang
                datasets: [{
                    label: 'Total Input',
                    data: totalInputAwasi, // Data total input
                    backgroundColor: [
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                    ],
                    borderColor: [
                        'rgba(75, 192, 192, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    title: {
                        display: true,
                        text: 'Top Bidang <?php echo $tahun ?>'
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            stepSize: 10
                        }
                    }
                }
            }
        });
  
    // If you want to draw your charts with Theme colors you must run initiating charts after that current skin is loaded
    $(window).bind("load", function () {
    	//-------------------------Initiates Easy Pie Chart instances in page--------------------//
        InitiateEasyPieChart.init();
    });
</script>