<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="" method="get" role="form">
                    <div class="form-group">
                        <select name="id_bidang" id="id_bidang" style="width:100%;" required="">
			                <option value="">--Pilih Bidang --</option>
			                <?php 
			                if ($this->session->userdata('level') == '7') {
			                	$id_pegawai = $this->session->userdata('keterangan');
			                	$id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
			                	$this->db->where('id_bidang', $id_bidang_user);
			                }
			                foreach ($this->db->get('bidang')->result() as $rw): 
			                    ?>
			                    <option value="<?php echo $rw->id_bidang ?>"><?php echo $rw->bidang ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>

                    <div class="form-group">
                    	<select name="bulan" id="bulan" style="width:100%;" required>
			                <option value="">--Pilih Bulan --</option>
			                <?php 
			                for ($i=1; $i <= 12; $i++) { 
			                	?>
			                	<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
			                	<?php
			                }
			                 ?>
			            </select>
                    </div>


                    <div class="form-group">
                    	<select name="tahun" id="tahun" style="width:100%;" required>
			                <option value="">--Pilih Tahun --</option>
			                <?php 
			                $this->db->order_by('tahun', 'desc');
			                // $this->db->where('tahun <=', date('Y'));
			                foreach ($this->db->get('tahun')->result() as $rw): 
			                    ?>
			                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
			                <?php endforeach ?>
			            </select>
                    </div>

                    
                    <button type="submit" class="btn btn-primary">FILTER</button>

                </form>
            </div>
        </div>
    </div>
</div>

<?php if (isset($_GET['id_bidang'])): 

$id_bidang = $this->input->get('id_bidang');
$bulan = $this->input->get('bulan');
$tahun = $this->input->get('tahun');

	?>

<div id="app">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12">
	        <div class="widget">
	            <div class="widget-header bordered-left bordered-darkorange">
	                <span class="widget-caption">History TTE</span>
	            </div>
	            <div class="widget-body bordered-left bordered-warning">

	            	<table class="table table-hover table-striped table-bordered">
	        			<tr>
	        				<td>Bidang</td>
	        				<td><b><?php echo get_data('bidang','id_bidang',$id_bidang,'bidang') ?></b></td>
	        			</tr>
	        			<tr>
	        				<td>Bulan</td>
	        				<td><b><?php echo bulan_indo($bulan) ?></b></td>
	        			</tr>
	        			<tr>
	        				<td>Tahun</td>
	        				<td><b><?php echo $tahun ?></b></td>
	        			</tr>

	        		</table>
	        	




	                <div class="table-scrollable">
		                <table class="table table-bordered table-hover table-striped">
		                    <thead class="bordered-darkorange">
		                        <tr role="row">
		                            <th width="50">No.</th>
		                            <th>Judul</th>
		                            <th>Dokumen</th>
		                            <th>Status</th>
		                            <th>Keterangan</th>
		                            <th>Pilihan</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    <?php 
		                    $no = 1;
		                    $query = "SELECT a.*, c.title as jenis_dokumen
								FROM tte_dokumen a
								INNER JOIN tte_jenis_dokumen c ON a.id_jenis_dokumen = c.id_doc
								WHERE a.bulan = $bulan AND a.tahun = $tahun AND a.id_bidang=$id_bidang
		                    ";
		                    $tte = $this->db->query($query);
		                    foreach ($tte->result() as $rw): ?>
		            
		                        <tr>
		                            <td><?php echo $no ?></td>
		                            <td><?php echo $rw->jenis_dokumen ?></td>
		                            <td><a href="https://tte-api.monevpupr.datakita.cloud/file/<?php echo $rw->file_name ?>" target="_blank">Lihat</a></td>
		                            <td>
		                            	<?php if ($rw->status == 'y'): ?>
		                            		<span class="label label-success">Selesai</span>
		                            	<?php endif ?>
		                            	<?php if ($rw->status == ''): ?>
		                            		<span class="label label-warning">Menunggu</span>
		                            	<?php endif ?>
		                            	<?php if ($rw->status == 't'): ?>
		                            		<span class="label label-danger">Ditolak</span>
		                            	<?php endif ?>

		                            </td>
		                            <td><?php echo $rw->keterangan ?></td>
		                            <td>
		                            	<?php if ($rw->status != 'y'): ?>
		                            		<a @click="onDelete('<?php echo $rw->id ?>')" class="label label-danger">Hapus</a>
		                            	<?php endif ?>
		                            </td>
		                            
		                            
		                        </tr>
		                        <tr>
		                        	<td colspan="6">
		                        		<table class="table table-hover">
		                        			<tr>
		                        				<th>Nama</th>
		                        				<th>Jabatan</th>
		                        				<th>Priority</th>
		                        				<th>Assign At</th>
		                        			</tr>
		                        			<?php 

		                        			$queryAssign = "
		                        			SELECT a.id, a.id_user, c.nama , a.priority, a.assign_at, d.jabatan
											FROM tte_assign_dokumen a INNER JOIN tte_user b ON a.id_user = b.id
											INNER JOIN pegawai c ON b.id_pegawai = c.id_pegawai
											INNER JOIN jabatan d ON d.id_jabatan = c.id_jabatan
											WHERE a.id_tte_dokumen = $rw->id
		                        			";
		                        			$tteAssign = $this->db->query($queryAssign);
		                        			foreach ($tteAssign->result() as $val):
		                        			 ?>
		                        			<tr>
		                        				<td><?php echo $val->nama ?></td>
		                        				<td><?php echo $val->jabatan ?></td>
		                        				<td><?php echo $val->priority ?></td>
		                        				<td><?php echo $val->assign_at ?></td>
		                        			</tr>
		                        			<?php endforeach ?>
		                        		</table>
		                        	</td>
		                        </tr>

		                    <?php $no++; endforeach ?>
		                        
		                    </tbody>
		                </table>
	            	</div>


	            </div>
	        </div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-lg-12 col-sm-12 col-xs-12">
	        <div class="widget">
	            <div class="widget-header bordered-left bordered-darkorange">
	                <span class="widget-caption">Upload TTE</span>
	            </div>
	            <div class="widget-body bordered-left bordered-warning">
	                <form class="form-inline" role="form" @submit.prevent="onSubmit()">

	                	<table class="table table-hover table-striped table-bordered">
	                		<tr>
	                			<th>Dokumen PDF</th>
	                			<td>
	                				<div class="form-group">
				                    	<input type="file" ref="file" class="form-control" @change="onFileUpload" accept="application/pdf" required>
				                    </div>
	                			</td>
	                		</tr>
	                		<tr>
	                			<th>Jenis Dokumen</th>
	                			<td>
	                				<div class="form-group">
				                    	<select name="id_jenis_dokumen" id="id_jenis_dokumen" style="width:100%;" v-model="formData.id_jenis_dokumen" required>
							                <option value="">-- Pilih Jenis Dokumen --</option>
							                <?php 
							                foreach ($this->db->get('tte_jenis_dokumen')->result() as $rw): 
							                    ?>
							                    <option value="<?php echo $rw->id_doc ?>"><?php echo $rw->title ?></option>
							                <?php endforeach ?>
							            </select>
				                    </div>
	                			</td>
	                		</tr>
	                		<tr>
	                			<th>Penanda Tangan</th>
	                			<td>
	                				
	                				<table class="table table-hover">
	                					<tr>
	                						<th>Nama</th>
	                						<th>Priority</th>
	                					</tr>
	                					<tr v-for="(entry, index) in formData.assign" :key="index">
	                						<td>
	                							<select class="form-control" style="width:100%;" v-model="entry.id_user" required>
									                <option value="">-- Pilih Pejabat --</option>
									                <?php 
									                $query = "
									                SELECT a.id, b.nama, c.jabatan
									                FROM tte_user a INNER JOIN pegawai b ON a.id_pegawai = b.id_pegawai
									                INNER JOIN jabatan c ON b.id_jabatan = c.id_jabatan
									                ";
									                foreach ($this->db->query($query)->result() as $rw): 
									                    ?>
									                    <option value="<?php echo $rw->id ?>"><?php echo $rw->nama .' - '.$rw->jabatan ?></option>
									                <?php endforeach ?>
									            </select>
	                						</td>
	                						<td>
	                							<input type="text" class="form-control" placeholder="Priority" v-model="entry.priority" required />
	                						</td>
	                					</tr>
	                					<tr>
	                						<td colspan="2">
	                							<button type="button" class="btn btn-info" @click="addEntry">Tambah Entri</button>
	                						</td>
	                					</tr>
	                				</table>

	                			</td>
	                		</tr>

	                
	                		
	                		<tr>
	                			<td></td>
	                			<td>
	                				<button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i>Upload</button>
	                			</td>
	                		</tr>
	                	</table>

	                </form>
	            </div>
	        </div>
	    </div>
	</div>
	
</div>



<script src="https://cdn.jsdelivr.net/npm/vue@2.7.14"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    pegawai: [],
   formData: {
				file: null,
				id_jenis_dokumen: '',
				assign: [{ id_user: '', priority: '' }]
			},
	assign: {
		data: []
	},
  },
  methods: {
  		addEntry() {
  			this.formData.assign.push({ id_user: '', priority: '' });
  			//console.log(this.formData.assign);
  		},
  		onFileUpload(event) {
			this.formData.file = event.target.files[0]
			//console.log(this.formData.file)
		},
		async onSubmit() {
			this.assign.data = this.formData.assign
			await axios
				.post('https://tte-api.monevpupr.datakita.cloud/api/v1/tte-dokumen',{
					id_jenis_dokumen: this.formData.id_jenis_dokumen,
					file_dokumen: this.formData.file,
					bulan: <?php echo $bulan ?>,
					tahun: <?php echo $tahun ?>,
					id_bidang: <?php echo $id_bidang ?>,
					assign: JSON.stringify(this.assign),
					created_by: '<?php echo $this->session->userdata('id_user') ?>',
					modified_by: '<?php echo $this->session->userdata('id_user') ?>',

				}, {
					headers: {
						'Content-Type': 'multipart/form-data',
						'X-Api': '<?php echo md5("emonevpuprtte") ?>'
					}
				})
				.then((res) => {
					//console.log(res)
					swal("Info", "Dokumen berhasil diupload, silahkan menunggu untuk di tanda tangani", "success")
					.then((value) => {
					  window.location.href = '<?php echo base_url().'tte?'.param_get() ?>';
					});

				})
				.catch((e) => {
					console.log(e)
					swal("Info", "Dokumen gagal diupload", "error");

				})
		},
		async onDelete(id) {
		    // Tambahkan konfirmasi sebelum menghapus
		    swal({
		        title: "Konfirmasi",
		        text: "Apakah Anda yakin ingin menghapus dokumen ini?",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
		    })
		    .then(async (willDelete) => {
		        if (willDelete) {
		            await axios
		                .delete('https://tte-api.monevpupr.datakita.cloud/api/v1/tte-dokumen/'+id, {
		                    headers: {
		                        'X-Api': '<?php echo md5("emonevpuprtte") ?>'
		                    }
		                })
		                .then((res) => {
		                    swal("Info", "Dokumen berhasil dihapus", "success")
		                    .then((value) => {
		                        window.location.href = '<?php echo base_url().'tte?'.param_get() ?>';
		                    });
		                })
		                .catch((e) => {
		                    console.log(e)
		                    swal("Info", "Dokumen gagal dihapus", "error");
		                });
		        } else {
		            swal("Info", "Penghapusan dokumen dibatalkan", "info");
		        }
		    });
		},
		
	},
 	 mounted() {
	}
});


$(document).ready(function() {
    // $("#pptk").select2();
    $("#pejabat").select2();
});

</script>

<?php endif ?>


