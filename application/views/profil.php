<?php 
$prof = $data->row();
 ?>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-lightred">
                <span class="widget-caption"><?php echo $judul_page ?></span>
            </div>
            <div class="widget-body">
            	<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

            	<?php 
            	$wajib_ubah_password = get_data('users','id_user',$this->session->userdata('id_user'),'wajib_ubah_password');
				if ($wajib_ubah_password == 'y') {
					echo alert_notif('Password anda sudah usang, silahkan update password anda !','warning');
				}
            	 ?>

            	<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
            		<div class="form-group">
	                    <label for="foto" class="col-sm-2 control-label no-padding-right">Foto Profil *</label>
	                    <div class="col-sm-10">
	                        <input type="file" class="form-control" name="foto">
	                        <div style="margin-top: 10px;">
	                        	<?php if ($prof->foto!=''): ?>
	                                <input type="hidden" name="foto_old" value="<?php echo $prof->foto ?>">
	                        		<img src="image/user/<?php echo $prof->foto ?>" style="width: 100px;">
	                        	<?php endif ?>
	                        </div>
	                        <p style="color: red">*)max ukuran file 1MB</p>
	                    </div>
	                    
	                </div>
            		<div class="form-group">
	                    <label class="col-sm-2 control-label no-padding-right">Password Sebelumnya </label>
	                    <div class="col-sm-10">
	                        <input type="password"  class="form-control" id="password_lama" name="password_lama" placeholder="Masukkan Password Lama">
	                    </div>
	                </div>

	                <div class="form-group">
	                    <label class="col-sm-2 control-label no-padding-right">Password Baru </label>
	                    <div class="col-sm-10">
	                        <input type="text"  class="form-control" id="password_baru" name="password_baru" placeholder="Masukkan Password Baru">
	                        <span id="password-error" style="color: red;"></span>
	                    </div>
	                    <div class="col-sm-12">
	                    	<span id="password-error" style="color: red;"></span>
	                    </div>
	                </div>
	                <div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-10">
                    		<button type="submit" class="btn btn-primary">Update</button>
                            
                    	</div>
                    </div>

            	</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	  // Fungsi untuk memeriksa apakah password memenuhi syarat
	  function validatePassword(password) {
	    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
	    return regex.test(password);
	  }

	  // Fungsi untuk menampilkan pesan error
	  function showPasswordError(message) {
	    $('#password-error').text(message);
	  }

	  // Ketika input field kehilangan fokus
	  $('#password_baru').keyup(function() {
	    var password = $(this).val();
	    if (!validatePassword(password)) {
	      showPasswordError('Password harus mengandung setidaknya 1 huruf kecil, 1 huruf besar, dan 1 angka. Panjang password minimal 8 karakter.');
	    } else {
	      showPasswordError('');
	    }
	  });
	});
</script>