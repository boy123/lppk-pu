<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Cetak Rekap Perkegiatan</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="cetak/cetak_rekap_perkegiatan" target="_blank" method="get" role="form">

                	<table class="table table-hover table-striped table-bordered">

                		<tr>
                			<th>Bulan</th>
                			<td>
                				<div class="form-group">
			                    	<select name="bulan" id="bulan" style="width:100%;" required>
						                <option value="">--Pilih Bulan --</option>
						                <?php 
						                for ($i=1; $i <= 12; $i++) { 
						                	?>
						                	<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
						                	<?php
						                }
						                 ?>
						            </select>
			                    </div>
                			</td>
                		</tr>
                		<tr>
                			<th>Tahun</th>
                			<td>
                				
                				<div class="form-group">
			                    	<select name="tahun" id="tahun" style="width:100%;" required>
						                <option value="">--Pilih Tahun --</option>
						                <?php 
						                $this->db->order_by('tahun', 'desc');
						                // $this->db->where('tahun <=', date('Y'));
						                foreach ($this->db->get('tahun')->result() as $rw): 
						                    ?>
						                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
						                <?php endforeach ?>
						            </select>
			                    </div>

                			</td>
                		</tr>

                		<tr>
                			<th>Bidang</th>
                			<td>
                				<div class="form-group">
			                        <select name="id_bidang" id="id_bidang" style="width:100%;" required="">
						                <option value="">--Pilih Bidang --</option>
						                <?php 
						                if ($this->session->userdata('level') == '7') {
						                	$id_pegawai = $this->session->userdata('keterangan');
						                	$id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
						                	$this->db->where('id_bidang', $id_bidang_user);
						                }
						                foreach ($this->db->get('bidang')->result() as $rw): 
						                    ?>
						                    <option value="<?php echo $rw->id_bidang ?>"><?php echo $rw->bidang ?></option>
						                <?php endforeach ?>
						            </select>
			                    </div>
                			</td>
                		</tr>

                		<tr>
                			<th>PPTK</th>
                			<td>
                				<div class="form-group">
			                        <select name="pptk" id="pptk" style="width:100%;" required="">
						                <option value=""><?php echo nbs(5) ?>--Pilih PPTK --<?php echo nbs(20) ?></option>
						                
						            </select>
			                    </div>
                			</td>
                		</tr>

                		<tr>
                			<th>Kegiatan</th>
                			<td>
                				<div class="form-group">
			                    	<select name="kegiatan" id="kegiatan" style="width:100%;">
						                <option value="">--Pilih Kegiatan --</option>
						            </select>
			                    </div>
                			</td>
                		</tr>


                        <tr>
                            <th>Jenis Cetak</th>
                            <td>
                                <div class="form-group">
                                    <select name="jenis_cetak" id="jenis_cetak" style="width:100%;" required>
                                        <option value="">--Jenis Cetak --</option>
                                        <option value="excel">EXCEL</option>
                                        <option value="pdf">PDF</option>
                                    </select>
                                </div>
                            </td>
                        </tr>

                		
                		<tr>
                			<td></td>
                			<td>
                				<button type="submit" class="btn btn-primary"><i class="fa fa-print"></i>Cetak</button>
                			</td>
                		</tr>
                	</table>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Cetak Rekap Perkegiatan Khusus</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="cetak/cetak_rekap_perkegiatan_khusus" target="_blank" method="get" role="form">

                	<table class="table table-hover table-striped table-bordered">

                		<tr>
                			<th>Bulan</th>
                			<td>
                				<div class="form-group">
			                    	<select name="bulan" id="bulan" style="width:100%;" required>
						                <option value="">--Pilih Bulan --</option>
						                <?php 
						                for ($i=1; $i <= 12; $i++) { 
						                	?>
						                	<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
						                	<?php
						                }
						                 ?>
						            </select>
			                    </div>
                			</td>
                		</tr>
                		<tr>
                			<th>Tahun</th>
                			<td>
                				
                				<div class="form-group">
			                    	<select name="tahun" id="tahun" style="width:100%;" required>
						                <option value="">--Pilih Tahun --</option>
						                <?php 
						                $this->db->order_by('tahun', 'desc');
						                // $this->db->where('tahun <=', date('Y'));
						                foreach ($this->db->get('tahun')->result() as $rw): 
						                    ?>
						                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
						                <?php endforeach ?>
						            </select>
			                    </div>

                			</td>
                		</tr>

                		<tr>
                			<th>Tipe</th>
                			<td>
                				<div class="form-group">
			                        <select name="tipe" id="tipe" style="width:100%;" required="">
						                <option value="">--Pilih Tipe --</option>
						                <option value="DBH">DBH</option>
						                <option value="(DAK)">DAK</option>
						                <option value="DAU">DAU</option>
						                
						            </select>
			                    </div>
                			</td>
                		</tr>

                        <tr>
                            <th>Jenis Cetak</th>
                            <td>
                                <div class="form-group">
                                    <select name="jenis_cetak" id="jenis_cetak" style="width:100%;" required>
                                        <option value="">--Jenis Cetak --</option>
                                        <option value="excel">EXCEL</option>
                                        <option value="pdf">PDF</option>
                                    </select>
                                </div>
                            </td>
                        </tr>

                		
                		<tr>
                			<td></td>
                			<td>
                				<button type="submit" class="btn btn-primary"><i class="fa fa-print"></i>Cetak</button>
                			</td>
                		</tr>
                	</table>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Search Kegiatan</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="cetak/search_kegiatan" target="_blank" method="get" role="form">

                	<table class="table table-hover table-striped table-bordered">

                		<tr>
                			<th>Bulan</th>
                			<td>
                				<div class="form-group">
			                    	<select name="bulan" id="bulan_search" style="width:100%;" required>
						                <option value="">--Pilih Bulan --</option>
						                <?php 
						                for ($i=1; $i <= 12; $i++) { 
						                	?>
						                	<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
						                	<?php
						                }
						                 ?>
						            </select>
			                    </div>
                			</td>
                		</tr>
                		<tr>
                			<th>Tahun</th>
                			<td>
                				
                				<div class="form-group">
			                    	<select name="tahun" id="tahun_search" style="width:100%;" required>
						                <option value="">--Pilih Tahun --</option>
						                <?php 
						                $this->db->order_by('tahun', 'desc');
						                // $this->db->where('tahun <=', date('Y'));
						                foreach ($this->db->get('tahun')->result() as $rw): 
						                    ?>
						                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
						                <?php endforeach ?>
						            </select>
			                    </div>

                			</td>
                		</tr>

                		<tr>
                			<th>Bidang</th>
                			<td>
                				<div class="form-group">
			                        <select name="id_bidang" id="id_bidang_search" style="width:100%;" required="">
						                <option value="">--Pilih Bidang --</option>
						                <?php 
						                if ($this->session->userdata('level') == '7') {
						                	$id_pegawai = $this->session->userdata('keterangan');
						                	$id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
						                	$this->db->where('id_bidang', $id_bidang_user);
						                }
						                foreach ($this->db->get('bidang')->result() as $rw): 
						                    ?>
						                    <option value="<?php echo $rw->id_bidang ?>"><?php echo $rw->bidang ?></option>
						                <?php endforeach ?>
						            </select>
			                    </div>
                			</td>
                		</tr>

                		<tr>
                			<th>Uraian</th>
                			<td>
                				<div class="form-group">
			                    	<select name="id_subkegiatan" id="kegiatan_search" style="width:100%;">
						                <option value="">--Pencarian--</option>
						            </select>
			                    </div>
                			</td>
                		</tr>

                		
                		<tr>
                			<td></td>
                			<td>
                				<button type="submit" class="btn btn-primary"><i class="fa fa-print"></i>Cetak</button>
                			</td>
                		</tr>
                	</table>

                </form>
            </div>
        </div>
    </div>
</div>


<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
	
$(document).ready(function() {
    $("#kegiatan").select2();
    $("#pptk").select2();

    $("#tahun").change(function() {
        var bidang = $("#id_bidang").val();
        var tahun = $(this).val();
       
        
    });

    $("#id_bidang").change(function() {
        var bidang = $("#id_bidang").val();
        var tahun = $("#tahun").val();
        
        getPPTK(bidang);
        
    });

    $("#pptk").change(function() {
        var pptk = $(this).val();
       	getListKegiatan(pptk);
    });

    $("#id_bidang_search").change(function() {
    	var bidang = $(this).val();
        var bulan = $("#bulan_search").val();
        var tahun = $("#tahun_search").val();
       	getListUraian(bidang, bulan, tahun);
    });

    function getPPTK(bidang) {
    	$.ajax({url: "dpa/get_pptk/"+bidang, 
            beforeSend: function(){
                $(".loading-container").show();
                $(".loader").show();
            },
            success: function(result){
                $("#pptk").html(result);
              console.log("success");
            },
            complete:function(data){
                $(".loading-container").hide();
                $(".loader").hide();
            }
        });
    }

    function getListKegiatan(id_user) {
    	$.ajax({url: "dpa/get_kegiatan_by_pptk/"+id_user, 
            beforeSend: function(){
                $(".loading-container").show();
                $(".loader").show();
            },
            success: function(result){
                $("#kegiatan").html(result);
              console.log("success");
            },
            complete:function(data){
                $(".loading-container").hide();
                $(".loader").hide();
            }
        });
    }

    function getListUraian(bidang, bulan, tahun) {
    	$.ajax({url: "dpa/get_uraian_by_bidang/"+bidang+"/"+bulan+"/"+tahun, 
            beforeSend: function(){
                $(".loading-container").show();
                $(".loader").show();
            },
            success: function(result){
                $("#kegiatan_search").html(result);
              console.log("success");
            },
            complete:function(data){
                $(".loading-container").hide();
                $(".loader").hide();
            }
        });
    }

    $("#kegiatan_search").select2();


});

</script>
