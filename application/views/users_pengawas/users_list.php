
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-yellow">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            <br>
            <div class="row">
            <div class="col-md-4">
                <?php echo anchor(site_url('users_pengawas/create'),'<i class="fa fa-plus"></i> Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <!-- <form action="<?php echo site_url('users/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('users'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form> -->
            </div>
        </div>
        <br>
        <div class="table-scrollable">
        <table class="table table-bordered table-hover table-striped" style="margin-bottom: 10px" id="searchable">
            <thead class="bordered-darkorange">
                <tr role="row">
                    <th>No</th>
        <th>Nama</th>
        <th>Username</th>
        <th>Kegiatan</th>
        <th>Bidang</th>
        <th>Tgl Mulai</th>
        <th>Tgl Selesai</th>
        <th>No HP</th>
        <th>Bypass Input</th>
        <!-- <th>Last Login</th>
        <th>Online</th> -->
        <th>Action</th>
                </tr>
            </thead>
            <tbody><?php
            $start = 1;
            $users_data = $this->db->get('users_pengawas')->result();
            foreach ($users_data as $users)
            {
                ?>
                <tr>
            <td width="80px"><?php echo $start ?></td>
            <td><?php echo $users->name ?></td>
            <td><?php echo $users->username ?></td>
            <td><?php echo get_data('dpa','id_dpa',$users->id_dpa,'uraian') ?></td>
            <td><?php echo get_data('bidang','id_bidang',$users->id_bidang,'bidang') ?></td>
            <td><?php echo $users->tanggal_mulai ?></td>
            <td><?php echo $users->tanggal_selesai ?></td>
            <td><?php echo $users->no_hp ?></td>
            <td><?php echo ($users->bypass == 1) ? "Active" : "Non Active" ?></td>
            <td style="text-align:center" width="200px">
                <?php if ($users->tanggal_mulai != '' AND $users->tanggal_selesai != ''): ?>
                    <?php 
                    $this->db->where('id_user', $users->id);
                    $this->db->where('id_dpa', $users->id_dpa);
                    $pekerjaan = $this->db->get('pekerjaan');

                    $this->db->where('id_user', $users->id);
                    $this->db->where('id_dpa', $users->id_dpa);
                    $laporan = $this->db->get('laporan_pengawas');
                    if ($pekerjaan->num_rows() == 0 AND $laporan->num_rows() == 0): ?>
                        <a onclick="javasciprt: return confirm('Yakin akan generate data diawasi, semua data sebelumnya akan terhapus')" href="users_pengawas/generate_laporan?id_user=<?php echo $users->id ?>&id_dpa=<?php echo $users->id_dpa ?>&start=<?php echo $users->tanggal_mulai ?>&end=<?php echo $users->tanggal_selesai ?>" class="label label-primary">Generate Awasi</a>
                    <?php else: ?>
                        <span class="label label-success">Done Generate</span>
                    <?php endif ?>
                    <a onclick="javasciprt: return confirm('Yakin akan perpanjang jadwal, pastikan tgl mulai dan selesai sesuai !')" href="users_pengawas/perpanjang_jadwal?id_user=<?php echo $users->id ?>&id_dpa=<?php echo $users->id_dpa ?>&start=<?php echo $users->tanggal_mulai ?>&end=<?php echo $users->tanggal_selesai ?>" class="label label-info">Perpanjang Jdwl</a>
                    
                <?php endif ?>
                <?php if ($users->bypass == 1): ?>
                    <a href="<?= site_url('users_pengawas/update_bypass/' . $users->id . '/0') ?>" class="label label-warning">UnBypass</a>
                <?php else: ?>
                    <a href="<?= site_url('users_pengawas/update_bypass/' . $users->id . '/1') ?>" class="label label-primary">Bypass</a>
                <?php endif ?>
                <?php 
                echo anchor(site_url('users_pengawas/update/'.$users->id),'<span class="label label-info">Ubah</span>'); 
                echo ' | '; 
                echo anchor(site_url('users_pengawas/delete/'.$users->id),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
                <?php
                $start++;
            }
            ?>
            </tbody>
        </table>
        </div>
        <br>
        <!-- <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div> -->
    </div>
        </div>
    </div>
</div>
    