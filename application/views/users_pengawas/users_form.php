
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
            </div>
        <div class="widget-body">
        <div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Nama * <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Username * <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" <?php echo $retVal = ($username != '') ? "readonly" : "" ; ?>/>
        </div>
	    <div class="form-group">
            <label for="varchar">Password * <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password"  />
            <input type="hidden" class="form-control" name="password_old" id="password" placeholder="Password" value="<?php echo $password; ?>" />
            <div>
                <?php if ($password != ''): ?>
                    <span>*) kosongkan password jika tidak dirubah</span>
                <?php endif ?>
            </div>
        </div>
	    
        <div class="form-group">
            <label for="int">Kegiatan * <?php echo form_error('id_dpa') ?></label>
            <select name="id_dpa" id="id_dpa"  style="width:100%;">
                <option value="">--Pilih Kegiatan --</option>
                <?php 
                $tahunnow = date("Y");
                $sql = "SELECT a.id_dpa, a.uraian 

                FROM dpa as a INNER JOIN subkegiatan as b ON a.id_subkegiatan = b.id_subkegiatan
                INNER JOIN kegiatan as c ON b.id_kegiatan = c.id_kegiatan
                WHERE c.tahun = $tahunnow

                ";
                foreach ($this->db->query($sql)->result() as $rw): 
                    $checked = ($id_dpa == $rw->id_dpa) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_dpa ?>" <?php echo $checked ?>><?php echo $rw->id_dpa.' - '.$rw->uraian ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label for="int">Bidang * <?php echo form_error('id_bidang') ?></label>
            <select name="id_bidang" id="id_bidang"  style="width:100%;">
                <option value="">--Pilih Bidang --</option>
                <?php 
                foreach ($this->db->get('bidang')->result() as $rw): 
                    $checked = ($id_bidang == $rw->id_bidang) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_bidang ?>" <?php echo $checked ?>><?php echo $rw->id_bidang.' - '.$rw->bidang ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label for="date">Tgl Mulai  <?php echo form_error('tanggal_mulai') ?></label>
            <div class="input-group">
                <input class="form-control date-picker" id="tanggal_mulai" type="text" name="tanggal_mulai" value="<?php echo $tanggal_mulai ?>" data-date-format="yyyy-mm-dd" autocomplete="off">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label for="date">Tgl Selesai <?php echo form_error('tanggal_selesai') ?></label>
            <div class="input-group">
                <input class="form-control date-picker" id="tanggal_selesai" type="text" name="tanggal_selesai" value="<?php echo $tanggal_selesai ?>" data-date-format="yyyy-mm-dd" autocomplete="off">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label for="varchar">No Hp * <?php echo form_error('no_hp') ?></label>
            <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="No Hp" value="<?php echo $no_hp; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('users_pengawas') ?>" class="btn btn-default">Cancel</a>
	</form>
                                    </div>
                                </div>
                                </div>

<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    $("#id_dpa").select2();
</script>
   