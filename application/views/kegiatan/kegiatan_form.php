
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
            </div>
        <div class="widget-body">
        <div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Kode Rekening <?php echo form_error('kode_rekening') ?></label>
            <input type="text" class="form-control" name="kode_rekening" id="kode_rekening" placeholder="Kode Rekening" value="<?php echo $kode_rekening; ?>" />
        </div>
	    <div class="form-group">
            <label for="kegiatan">Kegiatan <?php echo form_error('kegiatan') ?></label>
            <textarea class="form-control" rows="3" name="kegiatan" id="kegiatan" placeholder="Kegiatan"><?php echo $kegiatan; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="int">Bidang <?php echo form_error('id_bidang') ?></label>
            <select name="id_bidang" class="form-control" id="id_bidang">
                <option value="">--Pilih Bidang--</option>
                <?php 
                if ($this->session->userdata('level') == '7') {
                    $id_pegawai = $this->session->userdata('keterangan');
                    $id_bidang_user = get_data('pegawai','id_pegawai',$id_pegawai,'id_bidang');
                    $this->db->where('id_bidang', $id_bidang_user);
                }
                foreach ($this->db->get('bidang')->result() as $rw): 
                    $checked = ($id_bidang == $rw->id_bidang) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_bidang ?>" <?php echo $checked ?>><?php echo $rw->bidang ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="year">Tahun <?php echo form_error('tahun') ?></label>
            <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Tahun" value="<?php echo $tahun; ?>" />
        </div>
	    <input type="hidden" name="id_kegiatan" value="<?php echo $id_kegiatan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('kegiatan') ?>" class="btn btn-default">Cancel</a>
	</form>
                                    </div>
                                </div>
                                </div>

<script src="assets/js/select2/select2.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#id_bidang").select2();
    });
</script>