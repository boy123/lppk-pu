<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Cetak Rekap Dinas</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="cetak/cetak_rekap_dinas" target="_blank" method="get" role="form">

                	<table class="table table-hover table-striped table-bordered">
                		<tr>
                			<th>Bulan</th>
                			<td>
                				<div class="form-group">
			                    	<select name="bulan" id="bulan" style="width:100%;" required>
						                <option value="">--Pilih Bulan --</option>
						                <?php 
						                for ($i=1; $i <= 12; $i++) { 
						                	?>
						                	<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
						                	<?php
						                }
						                 ?>
						            </select>
			                    </div>
                			</td>
                		</tr>
                		<tr>
                			<th>Tahun</th>
                			<td>
                				
                				<div class="form-group">
			                    	<select name="tahun" id="tahun" style="width:100%;" required>
						                <option value="">--Pilih Tahun --</option>
						                <?php 
						                $this->db->order_by('tahun', 'desc');
						                // $this->db->where('tahun <=', date('Y'));
						                foreach ($this->db->get('tahun')->result() as $rw): 
						                    ?>
						                    <option value="<?php echo $rw->tahun ?>"><?php echo $rw->tahun ?></option>
						                <?php endforeach ?>
						            </select>
			                    </div>

                			</td>
                		</tr>
                        <tr>
                            <th>Jenis Cetak</th>
                            <td>
                                <div class="form-group">
                                    <select name="jenis_cetak" id="jenis_cetak" style="width:100%;" required>
                                        <option value="">--Jenis Cetak --</option>
                                        <option value="excel">EXCEL</option>
                                        <option value="pdf">PDF</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                		<tr>
                			<td></td>
                			<td>
                				<button type="submit" class="btn btn-primary"><i class="fa fa-print"></i>Cetak</button>
                			</td>
                		</tr>
                	</table>

                    


                    

                    
                    

                </form>
            </div>
        </div>
    </div>
</div>