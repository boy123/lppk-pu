<?php 
if ($this->input->get('jenis_cetak') == 'excel') {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap-Pengawas.xls");
}


 ?>



<html>

<head>
    <title>Cetak Rekap Pengawas</title>
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    body {
        margin: 0 auto;
    }

    body,
    td,
    th {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 12px;
    }

    th {
        text-align: center;
    }

    .nama_pt {
        font-size: 20px;
        font-weight: bold;
        line-height: 1.1;
        vertical-align: middle;
        text-align: center;
    }

    .info_pt {
        vertical-align: middle;
        text-align: center;
    }

    .kop {
        border-spacing: 0;
        border-collapse: collapse;
        border-bottom-style: double;
    }

    .side {
        width: 8%;
    }

    .img {
        width: 80px;
    }

    @media screen {
        .kop-width {
            width: 70%;
        }
    }

    @media print {
        .kop-width {
            width: 100%;
        }
    }

    @media screen {


        .kop-logo {
            width: 70%;
            margin: 0 auto;
        }

        .kop-logo img {
            width: 100%;
        }

        .custom-kop-html table {
            width: 70% !important;
            text-align: center !important;
        }
    }

    @media print {

        .kop-logo {
            width: 100% !important;
        }

        .kop-logo img {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <p class="navbar-brand">Cetak Rekap Pengawas</p>
            <button type="button" class="btn btn-primary btn-flat navbar-btn navbar-right"
                onclick="window.print(); return false;"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </nav>



    
    <br />
    <style type="text/css">
    @media screen {
        .table-name {
            width: 70%;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 70%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 70%;
            margin-right: 15%;
        }
    }

    @media print {
        @page {
            size: A4 portrait landscape;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 100%;
            margin-right: 0%;
        }

        .table-name {
            width: 100%;
        }
    }
    </style>

    <?php 
    $id_bidang = $this->input->get('id_bidang');
     ?>

    
    <table align="center" class="table-name">
        <!-- <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>REKAPITULASI</strong>
            </td>
        </tr> -->
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>Laporan Progress Pengawas Pekerjaan</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>DINAS PEKERJAAN UMUM PROVINSI JAMBI</strong>
            </td>
        </tr>
        <tr>
            <td colspan="8">&nbsp</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Tanggal</strong></td>
            <td align="left"><strong>:</strong> <?php echo tgl_indo(date('Y-m-d')) ?> </td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Bidang</strong></td>
            <td align="left"><strong>:</strong> <?php echo get_data('bidang','id_bidang',$id_bidang,'bidang') ?> </td>
        </tr>
        
        <!-- <tr>
            <td align="left" width="10%"><strong>Masa Pelaksanaan</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Penyedia</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>PPTK</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Pengawas</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr> -->
    </table>
    <br>
    <table class="table table-bordereds" width="100%" border="1|0" style="border-collapse: collapse;" align="center">
        <!-- header tabel -->
        <tr>
            <th rowspan="2" style="vertical-align:left;">NO</th>
            <th rowspan="2" style="vertical-align:left;">Pengawas</th>
            <th rowspan="2" style="vertical-align:left;">PEKERJAAN</th>
            <th rowspan="2" style="vertical-align:left;">MULAI KONTRAK</th>
            <th rowspan="2" style="vertical-align:left;">SELESAI KONTRAK</th>
            <th colspan="2" style="vertical-align:left;">KONSISTEN PELAPORAN</th>
            <th rowspan="2" style="vertical-align:left;">REALISASI (%)</th>

        </tr>
        <tr>
            <th style="vertical-align:center;">DI ISI</th>
            <th style="vertical-align:center;">TIDAK DI ISI</th>
        </tr>
        <tr>
            <th style="vertical-align:middle;">1</th>
            <th style="vertical-align:middle;">2</th>
            <th style="vertical-align:middle;">3</th>
            <th style="vertical-align:middle;">4</th>
            <th style="vertical-align:middle;">5</th>
            <th style="vertical-align:middle;">6</th>
            <th style="vertical-align:middle;">7</th>
            <th style="vertical-align:middle;">8</th>
        </tr>
        <!-- /header tabel -->

        <!-- isi table -->

        <?php 
        $no = 1;
        $sql = "SELECT 
                u.username,
                u.name,
                u.id_dpa,
                d.uraian as pekerjaan,
                u.tanggal_mulai,
                u.tanggal_selesai,
                u.name AS user_nama,
                (SELECT jl.realisasi_rencana  
                 FROM laporan_pengawas jl 
                 WHERE jl.id_user = u.id 
                   AND jl.jadwal_laporan <= CURDATE() 
                 ORDER BY jl.realisasi_rencana DESC 
                 LIMIT 1) AS realisasi_rencana,
                SUM(CASE WHEN jl.realisasi_rencana IS NULL OR jl.realisasi_rencana = '' THEN 1 ELSE 0 END) AS total_data_kosong,
                SUM(CASE WHEN jl.realisasi_rencana IS NOT NULL AND jl.realisasi_rencana != '' THEN 1 ELSE 0 END) AS total_terisi
            FROM 
                users_pengawas u
            INNER JOIN dpa d ON d.id_dpa = u.id_dpa 
            LEFT JOIN 
                laporan_pengawas jl ON u.id = jl.id_user 
            WHERE 
                jl.jadwal_laporan  <= CURDATE()
                AND u.id_bidang = $id_bidang
            GROUP BY 
                u.id, u.name
            ORDER BY u.name ASC";
        $dataLaporan = $this->db->query($sql)->result();
        foreach ($dataLaporan as $rw): 
            ?>
            
        
           
        <tr>
            
            <td align="center"><?php echo $no; ?></td>
            <td align="left"><?php echo $rw->name ?> </td>
            <td align="left"><?php echo $rw->pekerjaan ?> </td>
            <td align="center"><?php echo $rw->tanggal_mulai ?> </td>
            <td align="center"><?php echo $rw->tanggal_selesai ?> </td>
            <td align="right"><?php echo $rw->total_terisi ?></td>
            <td align="right"><?php echo $rw->total_data_kosong ?></td>
            <td align="right"><?php echo number_format($rw->realisasi_rencana, 2) ?></td>

        </tr>
        <?php
        $no++;
        endforeach ?>
            
        
    </table>
    <br />

    <table class="table-sign" style="display:none;" width="100%" align="right">
        <tr>
            <td width="70%"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center" width="30%">Jambi, <?php echo tgl_indo(date('Y-m-d')) ?></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <!-- <td align="center"> - </td> -->
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center">
                <!-- <strong>Muhammad Ridho, ST</strong> -->
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center">
                <!-- <span>NIP. 19830810 201101 1 006</span> -->
            </td>
            <td></td>
        </tr>
    </table>
    
</body>

</html>