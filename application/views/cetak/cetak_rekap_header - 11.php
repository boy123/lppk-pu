<html>

<head>
    <title>Cetak Rekap Bidang</title>
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    body {
        margin: 0 auto;
    }

    body,
    td,
    th {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 12px;
    }

    th {
        text-align: center;
    }

    .nama_pt {
        font-size: 20px;
        font-weight: bold;
        line-height: 1.1;
        vertical-align: middle;
        text-align: center;
    }

    .info_pt {
        vertical-align: middle;
        text-align: center;
    }

    .kop {
        border-spacing: 0;
        border-collapse: collapse;
        border-bottom-style: double;
    }

    .side {
        width: 8%;
    }

    .img {
        width: 80px;
    }

    @media screen {
        .kop-width {
            width: 70%;
        }
    }

    @media print {
        .kop-width {
            width: 100%;
        }
    }

    @media screen {


        .kop-logo {
            width: 70%;
            margin: 0 auto;
        }

        .kop-logo img {
            width: 100%;
        }

        .custom-kop-html table {
            width: 70% !important;
            text-align: center !important;
        }
    }

    @media print {

        .kop-logo {
            width: 100% !important;
        }

        .kop-logo img {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <p class="navbar-brand">Cetak Rekap Bidang</p>
            <button type="button" class="btn btn-primary btn-flat navbar-btn navbar-right"
                onclick="window.print(); return false;"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </nav>



    
    <br />
    <style type="text/css">
    @media screen {
        .table-name {
            width: 70%;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 70%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 70%;
            margin-right: 15%;
        }
    }

    @media print {
        @page {
            size: A4 portrait landscape;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 100%;
            margin-right: 0%;
        }

        .table-name {
            width: 100%;
        }
    }
    </style>

    <?php 
    $id_bidang = $this->input->get('id_bidang');
    $bulan = $this->input->get('bulan');
    $tahun = $this->input->get('tahun');
     ?>

    <table align="center" class="table-name">
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>REKAPITULASI</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>Laporan Perkembangan Pelaksanaan Kegiatan ( LPPK )</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>DINAS PEKERJAAN UMUM PROVINSI JAMBI</strong>
            </td>
        </tr>
        <tr>
            <td colspan="8">&nbsp</td>
        </tr>
        <tr>
	        <td align="left" width="5%"><strong>BIDANG</strong></td>
	        <td align="left"><strong>:</strong> <?php echo strtoupper(get_data('bidang','id_bidang',$id_bidang,'bidang')) ?> </td>
	    </tr>
	    <tr>
	    	<td align="left" width="5%"><strong>BULAN</strong></td>
	        <td align="left"><strong>:</strong> <?php echo strtoupper(bulan_indo($bulan)) ?> <?php echo $tahun ?></td>
	    </tr>
    </table>
    <br>
    <table class="table table-bordereds" width="80%" border="1|0" style="border-collapse: collapse;" align="center">
    	<!-- header tabel -->
        <tr>
            <th rowspan="4" style="vertical-align:middle;">No</th>
            <th rowspan="4" style="vertical-align:middle;">Uraian Kegiatan</th>
            <th rowspan="4" style="vertical-align:middle;">Total Dana Dalam APBD</th>
            <th rowspan="4" style="vertical-align:middle;">Total Dana Setelah Refocusing</th>
            <th rowspan="4" style="vertical-align:middle;">Total Dana Dalam APBD-P</th>
            <th colspan="5" style="text-align: center;">Perkembangan Pelaksanaan</th>
            <th rowspan="2" style="vertical-align:middle;">Sisa Anggaran</th>
        </tr>
        <tr>
            <th colspan="3" style="vertical-align:middle;">Realisasi Fisik</th>
            <th colspan="2" style="vertical-align:middle;">Daya Serap Keuangan</th>
        </tr>
        <tr>
            <th style="vertical-align:middle;">Target</th>
            <th style="vertical-align:middle;">Real</th>
            <th style="vertical-align:middle;">Dev</th>
            <th rowspan="2" style="vertical-align:middle;">(Rp.)</th>
            <th rowspan="2" style="vertical-align:middle;">%</th>
            <th rowspan="2" style="vertical-align:middle;">(Rp.)</th>
        </tr>
        <tr>
        	<th style="vertical-align:middle;">%</th>
        	<th style="vertical-align:middle;">%</th>
        	<th style="vertical-align:middle;">%</th>
        </tr>
        <tr>
        	<th style="vertical-align:middle;">1</th>
        	<th style="vertical-align:middle;">2</th>
        	<th style="vertical-align:middle;">3</th>
        	<th style="vertical-align:middle;">3</th>
        	<th style="vertical-align:middle;">3</th>
        	<th style="vertical-align:middle;">4</th>
        	<th style="vertical-align:middle;">5</th>
        	<th style="vertical-align:middle;">6</th>
        	<th style="vertical-align:middle;">7</th>
        	<th style="vertical-align:middle;">8</th>
        	<th style="vertical-align:middle;">9</th>
        </tr>
        <!-- /header tabel -->

        <!-- isi table -->

        <!-- <tr>
        	<td align="center">1</td>
        	<td align="lef">Koordinasi, Sinkronisasi dan Pelaksanaan Pemeliharaan Kawasan Rawa(1.03.02.1.02.29)</td>
        	<td align="right">1.000.245</td>
        	<td align="right">1.000.245</td>
        	<td align="right">1.000.245</td>
        	<td align="right">100,00</td>
        	<td align="right">98,00</td>
        	<td align="right">(0,97)</td>
        	<td align="right">1.000.245</td>
        	<td align="right">84,00</td>
        	<td align="right">1.000.245</td>
        </tr> -->

        <!-- /isi tabel -->

        <!-- footer tabel -->
        <tr>
        	<th colspan="2" style="vertical-align:middle;">TOTAL MURNI</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        </tr>
        <!-- /footer tabel -->

        
    </table>
    <br />
    
</body>

</html>