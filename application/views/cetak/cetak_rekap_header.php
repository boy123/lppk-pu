 <?php 
if ($this->input->get('jenis_cetak') == 'excel') {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap-Bidang.xls");
}


 ?>


<html>

<head>
    <title>Cetak Rekap Bidang</title>
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    body {
        margin: 0 auto;
    }

    body,
    td,
    th {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 12px;
    }

    th {
        text-align: center;
    }

    .nama_pt {
        font-size: 20px;
        font-weight: bold;
        line-height: 1.1;
        vertical-align: middle;
        text-align: center;
    }

    .info_pt {
        vertical-align: middle;
        text-align: center;
    }

    .kop {
        border-spacing: 0;
        border-collapse: collapse;
        border-bottom-style: double;
    }

    .side {
        width: 8%;
    }

    .img {
        width: 80px;
    }

    @media screen {
        .kop-width {
            width: 70%;
        }
    }

    @media print {
        .kop-width {
            width: 100%;
        }
    }

    @media screen {


        .kop-logo {
            width: 70%;
            margin: 0 auto;
        }

        .kop-logo img {
            width: 100%;
        }

        .custom-kop-html table {
            width: 70% !important;
            text-align: center !important;
        }
    }

    @media print {

        .kop-logo {
            width: 100% !important;
        }

        .kop-logo img {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <p class="navbar-brand">Cetak Rekap Bidang</p>
            <button type="button" class="btn btn-primary btn-flat navbar-btn navbar-right"
                onclick="window.print(); return false;"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </nav>



    
    <br />
    <style type="text/css">
    @media screen {
        .table-name {
            width: 70%;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 70%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 70%;
            margin-right: 15%;
        }
    }

    @media print {
        @page {
            size: A4 portrait landscape;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 100%;
            margin-right: 0%;
        }

        .table-name {
            width: 100%;
        }
    }
    </style>

    <?php 
    $id_bidang = $this->input->get('id_bidang');
    $bulan = $this->input->get('bulan');
    $tahun = $this->input->get('tahun');
     ?>

    <table align="center" class="table-name">
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>REKAPITULASI</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>Laporan Perkembangan Pelaksanaan Kegiatan ( LPPK )</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>DINAS PEKERJAAN UMUM DAN PERUMAHANAN RAKYAT PROVINSI JAMBI</strong>
            </td>
        </tr>
        <tr>
            <td colspan="8">&nbsp</td>
        </tr>
        <tr>
	        <td align="left" width="5%"><strong>BIDANG</strong></td>
	        <td align="left"><strong>:</strong> <?php echo strtoupper(get_data('bidang','id_bidang',$id_bidang,'bidang')) ?> </td>
	    </tr>
	    <tr>
	    	<td align="left" width="5%"><strong>BULAN</strong></td>
	        <td align="left"><strong>:</strong> <?php echo strtoupper(bulan_indo($bulan)) ?> <?php echo $tahun ?></td>
	    </tr>
    </table>
    <br>
    <table class="table table-bordereds" width="80%" border="1|0" style="border-collapse: collapse;" align="center">
    	<!-- header tabel -->
        <tr>
            <th rowspan="4" style="vertical-align:middle;">No</th>
            <th rowspan="4" style="vertical-align:middle;">Uraian Kegiatan</th>
            <th rowspan="4" style="vertical-align:middle;">Total Dana Dalam APBD</th>
            <th colspan="5" style="text-align: center;">Perkembangan Pelaksanaan</th>
            <th rowspan="2" style="vertical-align:middle;">Sisa Anggaran</th>
        </tr>
        <tr>
            <th colspan="3" style="vertical-align:middle;">Realisasi Fisik</th>
            <th colspan="2" style="vertical-align:middle;">Daya Serap Keuangan</th>
        </tr>
        <tr>
            <th style="vertical-align:middle;">Target</th>
            <th style="vertical-align:middle;">Real</th>
            <th style="vertical-align:middle;">Dev</th>
            <th rowspan="2" style="vertical-align:middle;">(Rp.)</th>
            <th rowspan="2" style="vertical-align:middle;">%</th>
            <th rowspan="2" style="vertical-align:middle;">(Rp.)</th>
        </tr>
        <tr>
        	<th style="vertical-align:middle;">%</th>
        	<th style="vertical-align:middle;">%</th>
        	<th style="vertical-align:middle;">%</th>
        </tr>
        <tr>
        	<th style="vertical-align:middle;">1</th>
        	<th style="vertical-align:middle;">2</th>
        	<th style="vertical-align:middle;">3</th>
        	<th style="vertical-align:middle;">4</th>
        	<th style="vertical-align:middle;">5</th>
        	<th style="vertical-align:middle;">6</th>
        	<th style="vertical-align:middle;">7</th>
        	<th style="vertical-align:middle;">8</th>
        	<th style="vertical-align:middle;">9</th>
        </tr>
        <!-- /header tabel -->

        <!-- isi table -->

        <?php 
        $no = 1;

        $this->db->where('id_bidang', $id_bidang);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
        $this->db->order_by('id_rekap_header', 'asc');
        foreach ($this->db->get('rekap_header')->result() as $rw): ?>

        <tr>
            <td align="center"><?php echo $no ?></td>
            <td align="lef">
                <?php 
                echo get_data('subkegiatan','id_subkegiatan',$rw->id_subkegiatan,'subkegiatan');
                echo '('.get_data('subkegiatan','id_subkegiatan',$rw->id_subkegiatan,'kode_rekening').')';
                 ?>
                    
            </td>
            <td align="right"><?php echo angka_indo(tot_dana_dpa($rw->id_rekap_header)) ?></td>
            <td align="right"><?php echo number_format(tot_persen_target($rw->id_rekap_header), 2) ?></td>
            <td align="right"><?php echo number_format(tot_persen_realisasi($rw->id_rekap_header), 2) ?></td>
            <td align="right"><?php
                $deviasi = tot_persen_realisasi($rw->id_rekap_header) - tot_persen_target($rw->id_rekap_header);
             echo number_format($deviasi, 2) ?></td>
            <td align="right"><?php echo angka_indo(tot_nilai_penyerapan($rw->id_rekap_header)) ?></td>
            <td align="right"><?php echo number_format(tot_persen_penyerapan($rw->id_rekap_header), 2) ?></td>
            <td align="right"><?php echo angka_indo(tot_sisa_anggaran($rw->id_rekap_header)) ?></td>
        </tr>
            
        <tr>
            <td></td>
            <td colspan="8">PPTK : <?php echo $rw->pptk ?></td>
        </tr>

            


        <?php $no++; endforeach ?>
        <!-- /isi tabel -->

        <!-- footer tabel -->
        <tr>
            <th colspan="2" style="vertical-align:middle;">TOTAL</th>
            <th style="vertical-align:middle;"><?php echo angka_indo(akum_dana_dpa($bulan, $tahun, $id_bidang)) ?></th>
            <th style="vertical-align:middle;"><?php echo number_format(akum_target_persentase($bulan, $tahun, $id_bidang), 2) ?></th>
            <th style="vertical-align:middle;"><?php echo number_format(akum_realisasi_persentase($bulan, $tahun, $id_bidang), 2) ?></th>
            <th style="vertical-align:middle;">
                <?php 
                $akum_deviasi = akum_realisasi_persentase($bulan, $tahun, $id_bidang) - akum_target_persentase($bulan, $tahun, $id_bidang);
                echo number_format($akum_deviasi,2);
                 ?>
            </th>
            <th style="vertical-align:middle;"><?php echo angka_indo(akum_nilai_penyerapan($bulan, $tahun, $id_bidang)) ?></th>
            <th style="vertical-align:middle;"><?php echo number_format(akum_persen_penyerapan($bulan, $tahun, $id_bidang), 2) ?></th>
            <th style="vertical-align:middle;"><?php echo angka_indo(akum_sisa_anggaran($bulan, $tahun, $id_bidang)) ?></th>
        </tr>
        <!-- /footer tabel -->

        
    </table>
    <br />

    <table class="table-sign" align="right">
        <tr>
            <td width="30%" align="center" colspan="3"></td>
            <td width="5%"></td>
            <td width="30%"></td>
            <td width="5%"></td>
            <td width="30%" align="center" colspan="2">Jambi, <?php echo tgl_indo(date('Y-m-d')) ?> </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp</td>
        </tr>
        <tr>
            <td align="center" colspan="3">Di ketahui oleh :</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center" colspan="3">Kuasa Pengguna Anggaran</td>
            <td></td>
            <td align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td></td>
            <td align="center" colspan="2">Bendahara Pengeluaran Pembantu</td>
        </tr>
        
        <!-- space ttd -->
        <tr>
            <td align="center" colspan="3"></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>

            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3">#2&nbsp;</td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td align="center" colspan="2">#1&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>

        <!-- batas space ttd -->

        <tr>
            <td align="center" colspan="3">
                <strong><?php echo getTTD('nama','3',$id_bidang) ?></strong>
            </td>
            <td></td>
            <td align="center">
                <strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
            </td>
            <td></td>
            <td align="center" colspan="2">
                <strong><?php echo getTTD('nama','2', $id_bidang) ?></strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">NIP. <?php echo getTTD('nip','3',$id_bidang) ?></td>
            <td></td>
            <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td></td>
            <td align="center" colspan="2">NIP. <?php echo getTTD('nip','2', $id_bidang) ?></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <!-- <tr>
            <td colspan="3">&nbsp;</td>
            <td align="center" colspan="3">Ka. Program Studi</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="center" style="border-bottom: 1px solid #000" colspan="3">
                <strong>Rahmat,M.Kom</strong>
            </td>
            <td align="center" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="center" colspan="3">NIP. 0415107603</td>
            <td align="center" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr> -->
    </table>
    
</body>

</html>