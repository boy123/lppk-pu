<?php 
if ($this->input->get('jenis_cetak') == 'excel') {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap-Pengawas.xls");
}


 ?>



<html>

<head>
    <title>Cetak Rekap Pengawas</title>
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    body {
        margin: 0 auto;
    }

    body,
    td,
    th {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 12px;
    }

    th {
        text-align: center;
    }

    .nama_pt {
        font-size: 20px;
        font-weight: bold;
        line-height: 1.1;
        vertical-align: middle;
        text-align: center;
    }

    .info_pt {
        vertical-align: middle;
        text-align: center;
    }

    .kop {
        border-spacing: 0;
        border-collapse: collapse;
        border-bottom-style: double;
    }

    .side {
        width: 8%;
    }

    .img {
        width: 80px;
    }

    @media screen {
        .kop-width {
            width: 70%;
        }
    }

    @media print {
        .kop-width {
            width: 100%;
        }
    }

    @media screen {


        .kop-logo {
            width: 70%;
            margin: 0 auto;
        }

        .kop-logo img {
            width: 100%;
        }

        .custom-kop-html table {
            width: 70% !important;
            text-align: center !important;
        }
    }

    @media print {

        .kop-logo {
            width: 100% !important;
        }

        .kop-logo img {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <p class="navbar-brand">Cetak Rekap Pengawas</p>
            <button type="button" class="btn btn-primary btn-flat navbar-btn navbar-right"
                onclick="window.print(); return false;"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </nav>



    
    <br />
    <style type="text/css">
    @media screen {
        .table-name {
            width: 70%;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 70%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 70%;
            margin-right: 15%;
        }
    }

    @media print {
        @page {
            size: A4 portrait landscape;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 100%;
            margin-right: 0%;
        }

        .table-name {
            width: 100%;
        }
    }
    </style>

    <?php 
    $bulan = $this->input->get('bulan');
    $tahun = $this->input->get('tahun');
    $id_user = $this->input->get('id_user');

    if ($bulan < 10) {
        $bulanQuery = '0'.$bulan;
    } else {
        $bulanQuery = $bulan;
    }

    $this->db->where('id', $id_user);
    $data = $this->db->get('users_pengawas')->row();

     ?>

    
    <table align="center" class="table-name">
        <!-- <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>REKAPITULASI</strong>
            </td>
        </tr> -->
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>Laporan Pengawas Pekerjaan</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>DINAS PEKERJAAN UMUM PROVINSI JAMBI</strong>
            </td>
        </tr>
        <tr>
            <td colspan="8">&nbsp</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Bulan</strong></td>
            <td align="left"><strong>:</strong> <?php echo strtoupper(bulan_indo($bulan)) ?> </td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Tahun</strong></td>
            <td align="left"><strong>:</strong> <?php echo $tahun ?> </td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Bidang</strong></td>
            <td align="left"><strong>:</strong> <?php echo get_data('bidang','id_bidang',$data->id_bidang,'bidang') ?> </td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Nama Pekerjaan</strong></td>
            <td align="left"><strong>:</strong> <?php echo get_data('dpa','id_dpa',$data->id_dpa,'uraian') ?> </td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Tanggal Kontrak</strong></td>
            <td align="left"><strong>:</strong> <?php echo $data->tanggal_mulai ?> / <?php echo $data->tanggal_selesai ?> </td>
        </tr>
        <!-- <tr>
            <td align="left" width="10%"><strong>Masa Pelaksanaan</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Penyedia</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>PPTK</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr>
        <tr>
            <td align="left" width="10%"><strong>Pengawas</strong></td>
            <td align="left"><strong>:</strong> -</td>
        </tr> -->
    </table>
    <br>
    <table class="table table-bordereds" width="100%" border="1|0" style="border-collapse: collapse;" align="center">
        <!-- header tabel -->
        <tr>
            <th rowspan="2" style="vertical-align:left;">Minggu Ke</th>
            <th rowspan="2" style="vertical-align:left;">Jadwal Input</th>
            <th rowspan="2" style="vertical-align:left;">Kegiatan Minggu ini</th>
            <th rowspan="2" style="vertical-align:left;">Kendala</th>

            <th colspan="3" style="text-align: center;">Perkembangan</th>
            <th rowspan="2" style="vertical-align:left;">Dokumentasi</th>

        </tr>
        <tr>
            <th style="vertical-align:center;">Target (%)</th>
            <th style="vertical-align:center;">Realisasi (%)</th>
            <th style="vertical-align:center;">Deviasi (%)</th>
        </tr>
        <tr>
            <th style="vertical-align:middle;">1</th>
            <th style="vertical-align:middle;">2</th>
            <th style="vertical-align:middle;">3</th>
            <th style="vertical-align:middle;">4</th>
            <th style="vertical-align:middle;">5</th>
            <th style="vertical-align:middle;">6</th>
            <th style="vertical-align:middle;">7</th>
            <th style="vertical-align:middle;">8</th>
        </tr>
        <!-- /header tabel -->

        <!-- isi table -->

        <?php 
        $no = 1;
        $dataLaporan = $this->db->query("SELECT * FROM laporan_pengawas where id_user='$id_user' and id_dpa=$data->id_dpa and jadwal_laporan like '$tahun-$bulanQuery-%'  ")->result();
        foreach ($dataLaporan as $rw): ?>
            
        
           
        <tr>
            <td align="center"><?php echo $no; ?></td>
            <td align="center"><?php echo $rw->jadwal_laporan ?> </td>
            <?php if ($rw->kegiatan_minggu_ini != ''): ?>
            <td align="left">
                <?php echo $rw->kegiatan_minggu_ini ?>                   
            </td>
            <td align="left"><?php echo $rw->kendala_dan_permasalahan ?></td>
            <td align="right"><?php echo $rw->bobot_rencana ?></td>
            <td align="right"><?php echo $rw->realisasi_rencana ?></td>
            <td align="right"><?php echo $rw->deviasi ?></td>
            <td align="center">
                <?php 
                $this->db->select('upload_dokumentasi');
                $this->db->where('id_laporan_pengawas', $rw->id);
                $this->db->limit(1);
                $imgs = $this->db->get('dokumentasi_pengawas');
                foreach ($imgs->result() as $img) {
                 ?>
                <img style="width: 300px" src="https://api.monevpupr.datakita.cloud/pengawas/<?php echo $img->upload_dokumentasi ?>">
                <?php } ?>
            </td>
            <?php else: ?>
            <td colspan="6" align="center">
                TIDAK ADA DATA                   
            </td>
            <?php endif ?>
        </tr>
        <?php
        $no++;
         endforeach ?>
            
        
    </table>
    <br />

    <table class="table-sign" style="display:none;" width="100%" align="right">
        <tr>
            <td width="70%"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center" width="30%">Jambi, <?php echo tgl_indo(date('Y-m-d')) ?></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center">PPTK</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <!-- <td align="center"> - </td> -->
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center">
                <!-- <strong>Muhammad Ridho, ST</strong> -->
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="center">
                <!-- <span>NIP. 19830810 201101 1 006</span> -->
            </td>
            <td></td>
        </tr>
    </table>
    
</body>

</html>