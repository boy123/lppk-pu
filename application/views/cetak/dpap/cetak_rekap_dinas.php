<?php 
// error_reporting(0);
 ?>

 <?php 
if ($this->input->get('jenis_cetak') == 'excel') {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap-Dinas.xls");
}


 ?>
<html>

<head>
    <title>Cetak Rekap Dinas</title>
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    body {
        margin: 0 auto;
    }

    body,
    td,
    th {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 12px;
    }

    th {
        text-align: center;
    }

    .nama_pt {
        font-size: 20px;
        font-weight: bold;
        line-height: 1.1;
        vertical-align: middle;
        text-align: center;
    }

    .info_pt {
        vertical-align: middle;
        text-align: center;
    }

    .kop {
        border-spacing: 0;
        border-collapse: collapse;
        border-bottom-style: double;
    }

    .side {
        width: 8%;
    }

    .img {
        width: 80px;
    }

    @media screen {
        .kop-width {
            width: 70%;
        }
    }

    @media print {
        .kop-width {
            width: 100%;
        }
        .warna-angka-kolom{
            background-color: grey !important;
        }
    }

    @media screen {


        .kop-logo {
            width: 70%;
            margin: 0 auto;
        }

        .kop-logo img {
            width: 100%;
        }

        .custom-kop-html table {
            width: 70% !important;
            text-align: center !important;
        }
    }

    @media print {

        .kop-logo {
            width: 100% !important;
        }

        .kop-logo img {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <p class="navbar-brand">Cetak Rekap Dinas</p>
            <button type="button" class="btn btn-primary btn-flat navbar-btn navbar-right"
                onclick="window.print(); return false;"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </nav>




    <br />
    <style type="text/css">
    @media screen {
        .table-name {
            width: 70%;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 70%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 70%;
            margin-right: 15%;
        }
    }

    @media print {
        @page {
            size: A4 portrait landscape;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 100%;
            margin-right: 0%;
        }

        .table-name {
            width: 100%;
        }
    }
    </style>

    <?php 
    $bulan = $this->input->get('bulan');
    $tahun = $this->input->get('tahun');
    $id_pegawai_session = $this->session->userdata('keterangan');
    $id_bidang = get_data('pegawai','id_pegawai',$id_pegawai_session, 'id_bidang');
     ?>

    <table align="center" class="table-name">
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>REKAPITULASI</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>LAPORAN PERKEMBANGAN PELAKSANAAN KEGIATAN ( LPPK )</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>DINAS PEKERJAAN UMUM DAN PERUMAHAN RAKYAT PROVINSI JAMBI</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>STATUS BULAN : <?php echo strtoupper(bulan_indo($bulan)) ?> <?php echo $tahun ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="8">&nbsp</td>
        </tr>

    </table>
    <br>
    <table class="table table-bordereds" width="80%" border="1|0" style="border-collapse: collapse;" align="center">
        <!-- header tabel -->
        <tr>
            <th rowspan="3" style="vertical-align:middle;">NO.</th>
            <th rowspan="3" style="vertical-align:middle;">BIDANG/UPTD</th>
            <th rowspan="2" style="vertical-align:middle;">JUMLAH DANA DALAM DPA</th>
            <th rowspan="2" style="vertical-align:middle;">JUMLAH DANA DALAM DPA-P</th>
            <th rowspan="2" style="vertical-align:middle;">BOBOT</th>
            <th colspan="5" style="text-align: center;">PERKEMBANGAN PELAKSANAAN KEGIATAN</th>
            <th rowspan="2" colspan="2" style="vertical-align:middle;">SISA ANGGARAN</th>
            <th rowspan="3" style="vertical-align:middle;">KET</th>
        </tr>
        <tr>
            <th colspan="2" style="vertical-align:middle;">DAYA SERAP KEUANGAN</th>
            <th colspan="3" style="vertical-align:middle;">FISIK</th>
        </tr>
        <tr>
            <th style="vertical-align:middle;">(Rp.)</th>
            <th style="vertical-align:middle;">(Rp.)</th>
            <th style="vertical-align:middle;">(%)</th>
            <th style="vertical-align:middle;">(Rp.)</th>
            <th style="vertical-align:middle;">(%)</th>
            <th style="vertical-align:middle;">Rencana (%)</th>
            <th style="vertical-align:middle;">Realisasi (%)</th>
            <th style="vertical-align:middle;">Deviasi (%)</th>
            <th style="vertical-align:middle;">(Rp.)</th>
            <th style="vertical-align:middle;">(%)</th>
        </tr>

        <tr>
            <th style="vertical-align:middle;">1</th>
            <th style="vertical-align:middle;">2</th>
            <th style="vertical-align:middle;">3</th>
            <th style="vertical-align:middle;">4</th>
            <th style="vertical-align:middle;">5</th>
            <th style="vertical-align:middle;">6</th>
            <th style="vertical-align:middle;">7</th>
            <th style="vertical-align:middle;">8</th>
            <th style="vertical-align:middle;">9</th>
            <th style="vertical-align:middle;">10</th>
            <th style="vertical-align:middle;">11</th>
            <th style="vertical-align:middle;">12</th>
            <th style="vertical-align:middle;">13</th>
        </tr>
        <!-- /header tabel -->

        <!-- isi table -->

        <?php 
        $tot_dana_dpa_p = 0;
        $akum_sisa_anggaran = 0;
        $akum_bobot = 0;
        $akum_nilai_penyerapan = 0;
        $akum_target_dinas = 0;
        $akum_realisasi_dinas = 0;
        $no = 1;
        $this->db->order_by('urutan', 'asc');
        $bidang = $this->db->get('bidang');
        foreach ($bidang->result() as $rw): ?>

        <tr>
            <td align="center"><?php echo $no ?></td>
            <td align="lef"><?php echo $rw->bidang ?></td>
            <td align="right">
                <?php 
                //$dana_dpa_dinas = dana_dpa_bidang_dinas($rw->id_bidang, $tahun);
                $dana_dpa_dinas = akum_dana_dpa($bulan, $tahun, $rw->id_bidang);
                echo angka_indo($dana_dpa_dinas) ?>
                    
            </td>
            <td align="right">
                <?php 
                $dana_dpap_dinas = akum_dana_dpa_p($bulan, $tahun, $rw->id_bidang);
                $tot_dana_dpa_p = $tot_dana_dpa_p + $dana_dpap_dinas;
                echo angka_indo($dana_dpap_dinas) ?>
                    
            </td>
            <td align="center">
                <?php 
                $bobot_dinas = bobot_dinas($bulan, $tahun, $rw->id_bidang);
                $akum_bobot = $akum_bobot + $bobot_dinas;
                echo number_format($bobot_dinas, 2) ?></td>
            <td align="right">
                <?php 
                $nilai_penyerapan_dinas = akum_nilai_penyerapan($bulan, $tahun, $rw->id_bidang);
                $akum_nilai_penyerapan = $akum_nilai_penyerapan + $nilai_penyerapan_dinas;
                echo angka_indo(akum_nilai_penyerapan($bulan, $tahun, $rw->id_bidang)) ?></td>
            <td align="right"><?php echo number_format(akum_persen_penyerapan_dpap($bulan, $tahun, $rw->id_bidang), 2) ?></td>
            <td align="right">
                <?php 
                $target_dinas = akum_target_persentase_dpap($bulan, $tahun, $rw->id_bidang);
                // $akum_target_dinas = $akum_target_dinas +$target_dinas;
                $akum_target_dinas = $akum_target_dinas + ( ($target_dinas * $bobot_dinas) / 100 );
                echo number_format($target_dinas, 2) ?>        
            </td>
            <td align="right">
                <?php 
                $realisasi_dinas = akum_realisasi_persentase_dpap($bulan, $tahun, $rw->id_bidang);
                //$akum_realisasi_dinas = $akum_realisasi_dinas + $realisasi_dinas;
                $akum_realisasi_dinas = $akum_realisasi_dinas + ( ( $realisasi_dinas * $bobot_dinas ) / 100 );
                echo number_format($realisasi_dinas, 2) ?>
                    
            </td>
            <td align="right">
                <?php 
                $deviasi = $realisasi_dinas - $target_dinas;
                echo number_format($deviasi, 2);
                 ?>
            </td>
            <td align="right">
                <?php 
                $sisa_anggaran = $dana_dpap_dinas - $nilai_penyerapan_dinas;
                $akum_sisa_anggaran = $akum_sisa_anggaran + $sisa_anggaran;
                echo angka_indo($sisa_anggaran);
                 ?>
            </td>
            <td align="right"></td>
            <td align="right"></td>
            
        </tr>

        <?php $no++; endforeach ?>

        <!-- /isi tabel -->

        <!-- footer tabel -->
        <tr>
            <th colspan="2" style="vertical-align:middle;">JUMLAH</th>
            <th style="text-align: right;"><?php echo angka_indo(akum_rekap_dinas($bulan, $tahun)) ?></th>
            <th style="vertical-align:middle;"><?php echo angka_indo($tot_dana_dpa_p) ?></th>
            <th style="vertical-align:middle;"><?php echo number_format($akum_bobot, 2) ?></th>
            <th style="text-align: right;"><?php echo angka_indo($akum_nilai_penyerapan) ?></th>
            <th style="vertical-align:middle;">
                <?php 
                $akum_persen_daya_serap = $akum_nilai_penyerapan/akum_rekap_dinas($bulan, $tahun) * 100;
                echo number_format($akum_persen_daya_serap, 2);
                 ?>
            </th>
            <th style="vertical-align:middle;">
                <?php 
                // $total_target_dinas = $akum_target_dinas/$akum_bobot*100;
                $total_target_dinas = $akum_target_dinas;
                echo number_format($total_target_dinas, 2);
                 ?>
            </th>
            <th style="vertical-align:middle;">
                <?php 
                $total_realisasi_dinas = $akum_realisasi_dinas/$akum_bobot*100;
                echo number_format($total_realisasi_dinas, 2);
                 ?>
            </th>
            <th style="vertical-align:middle;">
                
                <?php 
                $akum_deviasi = $total_realisasi_dinas - $total_target_dinas;
                echo number_format($akum_deviasi, 2);
                 ?>

            </th>
            <th style="text-align: right;"><?php echo angka_indo($akum_sisa_anggaran) ?></th>
            <th style="vertical-align:middle;">-</th>
            <th style="vertical-align:middle;">-</th>
        </tr>
        <!-- /footer tabel -->


    </table>
    <br />

    <br />
    <table class="table-sign" align="right">
        <tr>
            <td width="30%" align="center" colspan="3"></td>
            <td width="5%"></td>
            <td width="30%"></td>
            <td width="5%"></td>
            <td width="30%" align="center" colspan="2">Jambi, <?php echo tgl_indo(date('Y-m-d')) ?> </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp</td>
        </tr>
        <tr>
            <td align="center" colspan="3">Di ketahui oleh :</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center" colspan="3">Kepala Dinas Pekerjaan Umum dan Perumahan Rakyat Provinsi Jambi</td>
            <td></td>
            <td align="center">
                <?php 
                // if ($id_bidang != 3) {
                //     echo "Kepala Bidang <br>".ucwords(strtolower(get_data('bidang','id_bidang',$id_bidang,'bidang')));
                // } else {
                //     echo ucwords(strtolower(get_data('bidang','id_bidang',$id_bidang,'bidang')));
                // }

                //di ganti
                echo "Kepala Bidang <br> Bina Program dan Tata Ruang";

                 ?>
            </td>
            <td></td>
            <td align="center" colspan="2">Bendahara Pengeluaran</td>
        </tr>
        
        <tr>
            <td align="center" colspan="3"></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>

            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3">#3</td>
            <td></td>
            <td align="center">#2</td>
            <td>&nbsp;</td>
            <td align="center" colspan="2">#1</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>

            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>

        <tr>
            <td align="center" colspan="3">
                <strong><?php echo getTTD('nama','4') ?></strong>
            </td>
            <td></td>
            <td align="center">
                <strong><?php echo getTTD('nama','3',1) ?></strong>
            </td>
            <td></td>
            <td align="center" colspan="2">
                <strong><?php echo getTTD('nama','5') ?></strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">NIP. <?php echo getTTD('nip','4') ?></td>
            <td></td>
            <td align="center">NIP. <?php echo getTTD('nip','3',1) ?></td>
            <td></td>
            <td align="center" colspan="2">NIP. <?php echo getTTD('nip','5') ?></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <!-- <tr>
            <td colspan="3">&nbsp;</td>
            <td align="center" colspan="3">Ka. Program Studi</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="center" style="border-bottom: 1px solid #000" colspan="3">
                <strong>Rahmat,M.Kom</strong>
            </td>
            <td align="center" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="center" colspan="3">NIP. 0415107603</td>
            <td align="center" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr> -->
    </table>

</body>

</html>