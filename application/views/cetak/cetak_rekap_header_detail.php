<html>

<head>
    <title>Cetak Rekap Bidang Detail</title>
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    body {
        margin: 0 auto;
    }

    body,
    td,
    th {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 12px;
    }

    th {
        text-align: center;
    }

    .nama_pt {
        font-size: 20px;
        font-weight: bold;
        line-height: 1.1;
        vertical-align: middle;
        text-align: center;
    }

    .info_pt {
        vertical-align: middle;
        text-align: center;
    }

    .kop {
        border-spacing: 0;
        border-collapse: collapse;
        border-bottom-style: double;
    }

    .side {
        width: 8%;
    }

    .img {
        width: 80px;
    }

    @media screen {
        .kop-width {
            width: 70%;
        }
    }

    @media print {
        .kop-width {
            width: 100%;
        }
    }

    @media screen {


        .kop-logo {
            width: 70%;
            margin: 0 auto;
        }

        .kop-logo img {
            width: 100%;
        }

        .custom-kop-html table {
            width: 70% !important;
            text-align: center !important;
        }
    }

    @media print {

        .kop-logo {
            width: 100% !important;
        }

        .kop-logo img {
            width: 100%;
        }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <p class="navbar-brand">Cetak Rekap Bidang Detail</p>
            <button type="button" class="btn btn-primary btn-flat navbar-btn navbar-right"
                onclick="window.print(); return false;"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </nav>



    
    <br />
    <style type="text/css">
    @media screen {
        .table-name {
            width: 70%;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 70%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 70%;
            margin-right: 15%;
        }
    }

    @media print {
        @page {
            size: A4 portrait landscape;
        }

        .table-bordereds {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordereds td,
        .table-bordereds th {
            border: 1px solid #000 !important;
        }

        .table-sign {
            width: 100%;
            margin-right: 0%;
        }

        .table-name {
            width: 100%;
        }
    }
    </style>

    <?php 
    $id_bidang = $this->input->get('id_bidang');
    $bulan = $this->input->get('bulan');
    $tahun = $this->input->get('tahun');
     ?>

    <table align="center" class="table-name">
        <!-- <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>REKAPITULASI</strong>
            </td>
        </tr> -->
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>Laporan Perkembangan Pelaksanaan Kegiatan ( LPPK )</strong>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="8" style="font-size: 16px;">
                <strong>DINAS PEKERJAAN UMUM PROVINSI JAMBI</strong>
            </td>
        </tr>
        <tr>
            <td colspan="8">&nbsp</td>
        </tr>
        <tr>
	        <td align="left" width="5%"><strong>BIDANG</strong></td>
	        <td align="left"><strong>:</strong> <?php echo strtoupper(get_data('bidang','id_bidang',$id_bidang,'bidang')) ?> </td>
	    </tr>
	    <tr>
	    	<td align="left" width="5%"><strong>BULAN</strong></td>
	        <td align="left"><strong>:</strong> <?php echo strtoupper(bulan_indo($bulan)) ?> <?php echo $tahun ?></td>
	    </tr>
    </table>
    <br>
    <table class="table table-bordereds" width="100%" border="1|0" style="border-collapse: collapse;" align="center">
    	<!-- header tabel -->
        <tr>
            <th rowspan="2" style="vertical-align:middle;">No</th>
            <th rowspan="2" style="vertical-align:middle;">Uraian Kegiatan</th>
            <th rowspan="2" style="vertical-align:middle;">Total Dana Dalam APBD</th>
            <th rowspan="2" style="vertical-align:middle;">Volume</th>
            <th rowspan="2" style="vertical-align:middle;">Lokasi</th>
            <th rowspan="2" style="vertical-align:middle;">L/PL/ PML/SW</th>
            <th rowspan="2" style="vertical-align:middle;">Penyedia Jasa</th>
            <th rowspan="2" style="vertical-align:middle;">Nilai Kontrak</th>

            <th colspan="2" style="text-align: center;">Jangka Waktu</th>
            <th colspan="3" style="text-align: center;">Perkembangan</th>
            <th colspan="2" style="text-align: center;">Penyerapan</th>

            <th rowspan="2" style="vertical-align:middle;">Sisa Kontrak</th>
            <th rowspan="2" style="vertical-align:middle;">Sisa Anggaran</th>
            <th rowspan="2" style="vertical-align:middle;">Ket</th>
        </tr>
        <tr>
            <th style="vertical-align:center;">Mulai Kontrak</th>
            <th style="vertical-align:center;">Selesai Kontrak</th>
            <th style="vertical-align:center;">Target (%)</th>
            <th style="vertical-align:center;">Realisasi (%)</th>
            <th style="vertical-align:center;">Deviasi (%)</th>
            <th style="vertical-align:center;">Rp.</th>
            <th style="vertical-align:center;">(%) THD</th>
        </tr>
        <tr>
        	<th style="vertical-align:middle;">1</th>
        	<th style="vertical-align:middle;">2</th>
            <th style="vertical-align:middle;">3</th>
        	<th style="vertical-align:middle;">4</th>
        	<th style="vertical-align:middle;">5</th>
        	<th style="vertical-align:middle;">6</th>
        	<th style="vertical-align:middle;">7</th>
        	<th style="vertical-align:middle;">8</th>
            <th style="vertical-align:middle;">9</th>
            <th style="vertical-align:middle;">10</th>
            <th style="vertical-align:middle;">11</th>
            <th style="vertical-align:middle;">12</th>
            <th style="vertical-align:middle;">13</th>
            <th style="vertical-align:middle;">14</th>
            <th style="vertical-align:middle;">15</th>
            <th style="vertical-align:middle;">16</th>
            <th style="vertical-align:middle;">17</th>
            <th style="vertical-align:middle;">18</th>
        </tr>
        <!-- /header tabel -->

        <!-- isi table -->

        <?php 
        $no = 1;

        $this->db->where('id_bidang', $id_bidang);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
        $this->db->order_by('id_rekap_header', 'asc');
        foreach ($this->db->get('rekap_header')->result() as $rw): ?>
            
        <tr>
        	<td align="center"><?php echo $no ?></td>
        	<td align="lef">
                <?php 
                echo get_data('subkegiatan','id_subkegiatan',$rw->id_subkegiatan,'subkegiatan');
                echo '('.get_data('subkegiatan','id_subkegiatan',$rw->id_subkegiatan,'kode_rekening').')';
                 ?>
                    
            </td>
            <td align="right"><?php echo angka_indo(tot_dana_dpa($rw->id_rekap_header)) ?></td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"></td>
            <td align="right"><?php echo number_format(tot_persen_target($rw->id_rekap_header), 2) ?></td>
            <td align="right"><?php echo number_format(tot_persen_realisasi($rw->id_rekap_header), 2) ?></td>
            <td align="right"><?php
                $deviasi = tot_persen_realisasi($rw->id_rekap_header) - tot_persen_target($rw->id_rekap_header);
             echo number_format($deviasi, 2) ?></td>
            <td align="right"><?php echo angka_indo(tot_nilai_penyerapan($rw->id_rekap_header)) ?></td>
            <td align="right"><?php echo number_format(tot_persen_penyerapan($rw->id_rekap_header), 2) ?></td>
            <td align="right"></td>
            <td align="right"><?php echo angka_indo(tot_sisa_anggaran($rw->id_rekap_header)) ?></td>
        	<td align="right"></td>
        </tr>
            <?php 
            $this->db->where('id_rekap_header', $rw->id_rekap_header);
            $details = $this->db->get('rekap_detail');
             ?>
            <tr>
                <td rowspan="<?php echo $details->num_rows() + 1 ?>"></td>
                <td colspan="17">PPTK : <?php echo $rw->pptk ?></td>
            </tr>

            <?php 
            foreach ($details->result() as $detail): ?>
            <tr>
                <td><?php echo get_data('dpa','id_dpa',$detail->id_dpa,'uraian') ?></td>
                <td align="right"><?php echo angka_indo(get_data('dpa','id_dpa',$detail->id_dpa,'jumlah')) ?></td>
                <td align="center"><?php echo $detail->volume ?></td>
                <td align="left"><?php echo $detail->lokasi ?></td>
                <td align="center"><?php echo $detail->type ?></td>
                <td align="left"><?php echo $detail->penyedia_jasa ?></td>
                <td align="right"><?php echo angka_indo($detail->nilai_kontrak) ?></td>
                <td align="center"><?php echo $detail->mulai_kontrak ?></td>
                <td align="center"><?php echo $detail->selesai_kontrak ?></td>
                <td align="center"><?php echo $detail->target ?></td>
                <td align="center"><?php echo $detail->realisasi ?></td>
                <td align="center"><?php echo $detail->deviasi ?></td>
                <td align="right"><?php echo angka_indo($detail->nilai_penyerapan) ?></td>
                <td align="center"><?php echo $detail->persen_penyerapan ?></td>
                <td align="right"><?php echo angka_indo($detail->sisa_kontrak) ?></td>
                <td align="right"><?php echo angka_indo($detail->sisa_anggaran) ?></td>
                <td align="left"><?php echo $detail->ket ?></td>
            </tr>
            <?php endforeach ?>

        <?php $no++; endforeach ?>

        <!-- /isi tabel -->

        <!-- footer tabel -->
        <tr>
        	<th colspan="2" style="vertical-align:middle;">JUMLAH DANA APBD</th>
        	<th style="vertical-align:middle;"><?php echo angka_indo(akum_dana_dpa($bulan, $tahun, $id_bidang)) ?></th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
        	<th style="vertical-align:middle;">-</th>
            <th style="vertical-align:middle;">-</th>
            <th style="vertical-align:middle;">-</th>
            <th style="vertical-align:middle;"><?php echo number_format(akum_target_persentase($bulan, $tahun, $id_bidang), 2) ?></th>
            <th style="vertical-align:middle;"><?php echo number_format(akum_realisasi_persentase($bulan, $tahun, $id_bidang), 2) ?></th>
            <th style="vertical-align:middle;">
                <?php 
                $akum_deviasi = akum_realisasi_persentase($bulan, $tahun, $id_bidang) - akum_target_persentase($bulan, $tahun, $id_bidang);
                echo number_format($akum_deviasi,2);
                 ?>
            </th>
            <th style="vertical-align:middle;"><?php echo angka_indo(akum_nilai_penyerapan($bulan, $tahun, $id_bidang)) ?></th>
            <th style="vertical-align:middle;"><?php echo number_format(akum_persen_penyerapan($bulan, $tahun, $id_bidang), 2) ?></th>
            <th style="vertical-align:middle;">-</th>
            <th style="vertical-align:middle;"><?php echo angka_indo(akum_sisa_anggaran($bulan, $tahun, $id_bidang)) ?></th>
            <th style="vertical-align:middle;">-</th>
        </tr>
        <!-- /footer tabel -->

        
    </table>
    <br />
    
</body>

</html>